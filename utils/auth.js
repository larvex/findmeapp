import { AsyncStorage } from 'react-native'
import axios from 'react-native-axios';

const data = {
    grant_type: 'password',
    client_id: '****',
    client_secret: '****',
    scope: '*'
};

const url = 'http://findmeapp.mx';
//const url = 'http://192.168.0.31:1337';
module.exports =  Auth = {
    url,
    login: async (username, password) => {

        let d = {
            ...data,
            username,
            password
        }
        console.log('login user');

        return  axios.post(url+'/oauth/token', d);
    },
    register: async (username, name, password) => {

        let d = {
            ...data,
            username,
            name,
            password
        }
        console.log('register user');

        return  axios.post(url+'/api/register', d);
    },
    logout: async () => {
        console.log('logout user');

        await AsyncStorage.removeItem('@findme:token');
        return true;
    },
    getStates: async() => {
        console.log('get states');

        let instance = axios.create({
            baseURL: url+'/api/',
        });

        return instance({
            method:'get',
            url:'states',
        });
    },
    getMyDogs: async(tok) => {
        console.log('get my dogs');

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok}
        });

        return instance({
            method:'get',
            url:'user/dogs',
        });

    },
    getNotifications: async(tok) => {
        console.log('get my notifications');

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok}
        });

        return instance({
            method:'get',
            url:'notifications/get',
        });

    },
    //FINDLOVE
    findLoveList: async(dog, tok) =>{
      console.log('get tinder list usin dog info');

      let instance = axios.create({
          baseURL: url+'/api/',
          headers: {'Authorization': 'Bearer '+tok}
      });

      return instance({
          method:'post',
          url:'findlove/list',
          data:dog
      });
    },
    sendMatchRequest: async(r_pet_id, t_pet_id, tok) => {
        console.log('send match request with target pet info');

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok}
        });

        return instance({
            method:'post',
            url:'findlove/request/send',
            data:{
                r_pet_id,
                t_pet_id
            }
        });
    },
    findHomeList: async(dog, tok) =>{
        console.log('get home list usin dog info');

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok}
        });

        return instance({
            method:'post',
            url:'findhome/list',
            data:dog
        });
    },
    sendAdoptionRequest: async(t_pet_id, tok) => {
        console.log('send adoption request with target pet info');

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok}
        });

        return instance({
            method:'post',
            url:'findhome/request/send',
            data:{
                t_pet_id
            }
        });
    },
    sendChatRequest: async(t_user_id, tok) => {
        console.log('send chat request to user');

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok}
        });

        return instance({
            method:'post',
            url:'chat/request/send',
            data:{
                t_user_id
            }
        });
    },
    getChatList: async(tok) => {
        console.log('get active chats');

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok}
        });

        return instance({
            method:'get',
            url:'chats/get'
        });
    },
    dropDog: async(id, tok) => {
        console.log('drop dog data using bearer',id);

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok}
        });

        return instance({
            method:'get',
            url:'user/pets/'+id+'/drop'
        });
    },
    saveFoundAlert: async(alert_data, tok) => {
        console.log('save found alert data using bearer',alert_data);

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok, 'content-type': 'multipart/form-data'}
        });

        return instance({
            method:'post',
            url:'pet/foundalert/save',
            data:alert_data
        });
    },
    saveDog: async(dog_data, tok) => {
        console.log('save dog data using bearer',dog_data);

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok, 'content-type': 'multipart/form-data'}
        });

        return instance({
            method:'post',
            url:'user/pets/save',
            data:dog_data
        });
    },
    createAlert: async(alert_data, tok) => {
        console.log('save alert using bearer',alert_data);

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok}
        });

        return instance({
            method:'post',
            url:'pet/alert/save',
            data:alert_data
        });
    },
    getChatRoom: async(room_data, tok) => {
        console.log('check room using bearer',room_data);

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok}
        });

        return instance({
            method:'post',
            url:'chat_room/get',
            data:room_data
        });
    },
    closeAlert: async(alert_data, tok) => {
        console.log('save alert using bearer',alert_data);

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok}
        });

        return instance({
            method:'post',
            url:'pet/alert/close',
            data:alert_data
        });
    },
    updatePic: async(pic, tok) => {
        console.log('update pic using bearer',pic);

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok, 'content-type': 'multipart/form-data'}
        });

        return instance({
            method:'post',
            url:'user/pic/update',
            data:pic
        });
    },
    updateDog: async(dog_data, tok) => {
        console.log('update dog data using bearer',dog_data);

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok, 'content-type': 'multipart/form-data'}
        });

        return instance({
            method:'post',
            url:'user/pets/update',
            data:dog_data
        });
    },
    saveProfile: async(user_data, tok) => {
        console.log(user_data, tok);
        console.log('save user data using bearer');

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok}
        });

        return instance({
            method:'post',
            url:'user',
            data:user_data
        });
    },
    saveItem: async (item, selectedValue) => {
        return AsyncStorage.setItem(item, selectedValue);
    },
    removeItem: async (item) => {
        return AsyncStorage.removeItem(item);
    },
    getItem: async (item) => {
        return AsyncStorage.getItem(item);
    },
    getMe: async (tok) => {
        console.log('Get user data using bearer');

        let instance = axios.create({
            baseURL: url+'/api/',
            headers: {'Authorization': 'Bearer '+tok}
        });

        return instance({
            method:'get',
            url:'user',
        });

    }

};
/*
getItemF = async (item) => {

    try {
        let token =  await AsyncStorage.getItem(item);
        console.log('get item from local storage: '+item);

        if (token !== null){
            return token;
        }
        return false;
    } catch (error) {
        console.log('AsyncStorage error: ' + error.message);
    }
};

saveItemF = async (item, selectedValue) => {
    try {
        await AsyncStorage.setItem(item, selectedValue);
    } catch (error) {
        console.log('AsyncStorage error: ' + error.message);
    }
};

removeItemF = async (item) => {
    try {
        await AsyncStorage.removeItem(item);
        return true;
    } catch (error) {
        console.error('AsyncStorage error: ' + error.message);
    }
};
    */