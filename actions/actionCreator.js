import {
    Login,
    LoginTry,
    Logout,
    Register,
    RegisterTry,
    ServicePending,
    ServiceSuccess,
    ServiceError
} from "./actionTypes";

const login = (username, password) => ({
    type: Login,
    username,
    password
});

const login_try = () => ({
    type: LoginTry
});

const logout = () => ({
    type: Logout
});

const register = () => ({
    type: Register
});

const register_try = () => ({
    type: RegisterTry
});

const service_pending = () => ({
   type: ServicePending
});

const service_success = (data) => ({
    type: ServiceSuccess,
    data: {...data}
});

const service_error = () => ({
    type: ServiceError
});

export { login, login_try, logout, register, register_try, service_pending, service_success, service_error };