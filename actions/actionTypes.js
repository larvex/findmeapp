const Login = "LOGIN";
const LoginTry = "LOGIN_TRY";
const Logout = "LOGOUT";
const Register = "REGISTER";
const RegisterTry = "REGISTER_TRY";
const ServicePending = "SERVICE_PENDING";
const ServiceError = "SERVICE_ERROR";
const ServiceSuccess = "SERVICE_SUCCESS";

export { Login, LoginTry, Logout, Register, RegisterTry, ServicePending, ServiceError, ServiceSuccess };