import { NavigationActions } from "react-navigation";
import AppNavigator from "../navigator/navigationStack";

import { Login, LoginTry, Logout, RegisterTry, Register } from "../actions/actionTypes";


const ActionForLoggedOut = AppNavigator.router.getActionForPathAndParams("Login");

const ActionForLoggedIn = AppNavigator.router.getActionForPathAndParams("AppMain");

const stateForLoggedOut = AppNavigator.router.getStateForAction(
    NavigationActions.reset({
        index: 0,
        actions: [
            NavigationActions.navigate({
                routeName: "Login",
            }),
        ],
    })
);
const stateForLoggedIn = AppNavigator.router.getStateForAction(
    NavigationActions.reset({
        index: 0,
        actions: [
            NavigationActions.navigate({
                routeName: "AppMain",
            }),
        ],
    })
);

const initialState = { stateForLoggedOut, stateForLoggedIn };

const navigationReducer = (state = initialState, action) => {
    switch (action.type) {
        case "@@redux/INIT":
            return {
                ...state,
                stateForLoggedIn: AppNavigator.router.getStateForAction(
                    ActionForLoggedIn,
                    stateForLoggedOut
                )
            };

        case Login:
            return {
                ...state,
                stateForLoggedIn: AppNavigator.router.getStateForAction(
                    NavigationActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: "AppMain" })]
                    })
                )
            };

        case LoginTry:
            return {
                ...state,
                stateForLoggedOut: AppNavigator.router.getStateForAction(
                    NavigationActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: "Login" })]
                    })
                )
            };

        case Logout:
            return {
                ...state,
                stateForLoggedOut: AppNavigator.router.getStateForAction(
                    NavigationActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: "Login" })]
                    })
                )
            };

        case RegisterTry:
            return {
                ...state,
                stateForLoggedOut: AppNavigator.router.getStateForAction(
                    NavigationActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: "Signup" })]
                    })
                )
            };

        default:
            return {
                ...state,
                stateForLoggedIn: AppNavigator.router.getStateForAction(
                    action,
                    state.stateForLoggedIn
                )
            };
    }
};

export default navigationReducer;