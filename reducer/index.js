import { combineReducers } from "redux";
import NavigationReducer from "./navigationReducer";
import LoginReducer from "./loginReducer";

const AppReducer = combineReducers({
    LoginReducer,
    NavigationReducer
});

export default AppReducer;