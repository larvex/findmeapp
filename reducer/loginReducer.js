import {Login, Logout, RegisterTry, Register, ServiceError, ServicePending, ServiceSuccess} from "../actions/actionTypes";

const initialState = {
    isLoggedIn: false,
    isLoading: false,
    error: false,
    data: {
    }
};

const loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case Login:
            return {
                ...state,
                isLoggedIn: true
            };

        case Logout:
            return {
                ...state,
                isLoggedIn: false,
                username: '',
                password: '',
                data: {}
            };

        case ServicePending:
            return {
                ...state,
                isLoading: true
            };
        case ServiceError:
            return {
                ...state,
                isLoading:false,
                error: action.error
            };
        case ServiceSuccess:
            let d = action.data
            return {
                ...state,
                isLoading:false,
                data: {...state.data, ...d}
            };
        default:
            return state;
    }
};

export default loginReducer;