/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, {Component} from 'react';
import {Text, View, StyleSheet, ScrollView} from 'react-native';
import {Provider} from 'react-redux';
import SplashScreen  from './pages/splash-screen';
import AppNavigation from './navigator/AppNavigator';
import configureStore from "./store";
import Toast from 'react-native-simple-toast';

console.disableYellowBox = true;
const {store} = configureStore();

export default class App extends Component {


    render() {
        return (
            <Provider store={store}>
                <AppNavigation />
            </Provider>
        );
    }
}
