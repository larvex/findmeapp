import {StackNavigator, TabNavigator} from 'react-navigation';
import LoginScreen  from '../pages/login-screen';
import SignupScreen from '../pages/signup-screen';
import ProfilePicEditScreen  from '../pages/profilepic-edit-screen';    //CAMBIAR FOTO DE PERFIL
import ProfileEditScreen  from '../pages/profile-edit-screen';          //CAMBIAR DATOS DE PERFIL
import ProfileScreen  from '../pages/profile-screen';                   //PERFIL DE USUARIO

import FindMeChoiceScreen  from '../pages/findme-choice-screen';        //ELEGIR SI SE ENCONTRO MASCOTA O SE PERDIO
import FindMePetsScreen  from '../pages/findme-dogs-screen';            //ELIGE LA MASCOTA EXTRAVIADA
import FindMeAlertScreen  from '../pages/findme-alert-screen';          //DATOS DE LA MASCOTA EXTRAVIADA
import FindMeAlertMapScreen  from '../pages/findme-alert-map-screen';   //UBICACION DE LA MASCOTA EXTRAVIADA
import FoundPetScreen from '../pages/found-capture-screen';               //DATOS DE MASCOTA ENCONTRADA



import PetsScreen  from '../pages/dog-list-screen';                     //LISTADO DE MASCOTAS
import DogCaptureScreen from '../pages/dog-capture-screen';             //AGREGAR MASCOTA
import DogEditScreen from '../pages/dog-edit-screen';                   //EDITAR MASCOTA
import CameraRollScreen from '../pages/camera-roll-screen';

import FindLoveChoiceScreen from '../pages/findlove-choice-screen';
import FindLoveScreen from '../pages/tinder-list-screen';

import FindHomeChoiceScreen from '../pages/findhome-choice-screen';
import FindHomeScreen from '../pages/home-list-screen';


import NotificationScreen from '../pages/notification-screen';
import ChatScreen from '../pages/chat-screen';
import MessagesScreen from '../pages/messages-screen';
import { Platform } from 'react-native';

const FindMeMain = TabNavigator({
    Profile: {
        screen: ProfileScreen
    },
    Pets: {
        screen: PetsScreen
    },
    FindMe: {
        screen: FindMeChoiceScreen
    },
    FindLove: {
        screen: FindLoveChoiceScreen
    },
    FindHome: {
        screen: FindHomeChoiceScreen
    },
}, {
    headerMode: 'none',
    animationEnabled: false,
    tabBarPosition: 'bottom',
    swipeEnabled: false,
    lazy:true,
    tabBarOptions: {
        indicatorStyle: {
            backgroundColor: '#be2040',
        },
        activeTintColor: '#be2040',
        activeBackgroundColor: '#202020',
        inactiveTintColor: '#fff',
        inactiveBackgroundColor: '#202020',
        showIcon: 'true',
        showLabel: (Platform.OS !== 'android'), //No label for Android
        labelStyle: {
            fontSize: 11,
        },
        style: {
            backgroundColor: '#202020', // Makes Android tab bar white instead of standard blue
        }
    },
});
export default nav = StackNavigator({
    Login: {
        screen: LoginScreen,
    },
    Signup: {
        screen: SignupScreen
    },
    AppMain: {
        screen: FindMeMain,
    },
    ProfileEdit: {
        screen: ProfileEditScreen,
    },
    ProfilePicEdit: {
        screen: ProfilePicEditScreen,
    },
    AddPet: {
        screen: DogCaptureScreen
    },
    EditPet: {
        screen: DogEditScreen
    },
    FindMePets: {
        screen: FindMePetsScreen
    },
    FindMeAlert: {
        screen: FindMeAlertScreen
    },
    FindMeAlertMap: {
        screen: FindMeAlertMapScreen
    },
    FoundPet: {
        screen: FoundPetScreen
    },
    Notifications: {
        screen: NotificationScreen,
    },
    Chat: {
        screen: ChatScreen,
    },
    Messages: {
        screen: MessagesScreen,
    },
    CameraRollS:{
        screen: CameraRollScreen
    },
    FindLoveResults:{
        screen: FindLoveScreen
    },
    FindHomeResults:{
        screen: FindHomeScreen
    }
});
