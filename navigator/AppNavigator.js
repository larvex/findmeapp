import React, {Component} from 'react';
import {BackHandler, View, ActivityIndicator} from "react-native";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {NavigationActions, addNavigationHelpers} from 'react-navigation';
import NavigationStack from "./navigationStack";
import * as ActionCreator from "../actions/actionCreator";
import Auth from '../utils/auth';
const styles = require("./../assets/styles/styles");

class AppNavigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            splash: true
        };
    }


    componentDidMount() {
        console.log('Navigator Mounted');
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

        Auth.getItem('@findme:token')
            .then(response => {
                if (response) {
                    console.log('Got a Token from storage');
                    this.props.service_success({bearer: response});
                    Auth.getMe(response)
                        .then(resp => {
                            console.log(resp);
                            this.props.service_success(resp.data);
                            this.props.login('', '');
                            this.setState({
                                splash: false
                            });
                        }).catch(error => {
                        console.log('Got tokken but couldnt get user: ', error);
                        Auth.removeItem('@findme:token', '').then(() => {
                            this.setState({
                                splash: false
                            });
                        }).catch(error => {
                            console.log(error.response);
                            Toast.show('Ocurrió un error al iniciar sesión.', Toast.LONG);

                        });
                    });
                } else {
                    console.log('Got null token from storage');
                    this.setState({
                        splash: false
                    });
                }

            }).catch(error => {
            console.log(error.response);
            Toast.show('Ocurrió un error al iniciar sesión.', Toast.LONG);

        });


    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        const {dispatch, navigationState} = this.props;
        if (navigationState.stateForLoggedIn.index <= 1) {
            BackHandler.exitApp();
            return;
        }
        dispatch(NavigationActions.back());
        return true;
    };

    render() {
        const {navigationState, dispatch, isLoggedIn} = this.props;
        const state = isLoggedIn
            ? navigationState.stateForLoggedIn
            : navigationState.stateForLoggedOut;

        if (this.state.splash) {
            return (
                <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>
            );
        } else {
            return (
                <NavigationStack navigation={addNavigationHelpers({dispatch, state})}/>
            );
        }

    }
}

const mapStateToProps = state => {
    return {
        isLoggedIn: state.LoginReducer.isLoggedIn,
        navigationState: state.NavigationReducer
    };
};
const mapDispatchToProps = (dispatch) => {
    return Object.assign(
        {dispatch: dispatch},
        bindActionCreators(ActionCreator, dispatch)
    );

};

export default connect(mapStateToProps, mapDispatchToProps)(AppNavigation);