import React, {Component} from 'react';
import {connect} from "react-redux";
import {StyleSheet, View, Image, Text, KeyboardAvoidingView, ActivityIndicator, ScrollView} from 'react-native';
import {Button, H1, H2, H3, Container, Item, Icon, Input, Content, Footer} from 'native-base';
import {Col, Row, Grid} from "react-native-easy-grid";
import {login, register_try, service_success, service_error, service_pending, login_try} from "../actions/actionCreator";
import Auth from '../utils/auth';
import Toast from 'react-native-simple-toast';

const styles = require("./../assets/styles/styles");


class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    static navigationOptions = {
        header: null
    };

    userLogin() {
        if (!this.state.username || !this.state.password) {
            Toast.show('Debes llenar todos los campos.', Toast.LONG);
            return;
        }
        this.props.service_pending();

        let tok = null;
        let data = {};

        Auth.login(this.state.username, this.state.password)
            .then((response) => {
                tok = response.data.access_token;
                console.log('User Logged', tok);

                Auth.getMe(tok)
                    .then(resp => {
                        data = resp.data;
                        data.bearer = tok;
                        console.log('got data', data);

                        Auth.saveItem('@findme:token', tok).then(() => {

                            this.props.service_success(data);
                            this.props.login('', '');


                        }).catch(error => {
                            console.log(error.response);
                            this.props.service_error();
                            Toast.show('Ocurrió un error al iniciar sesión.', Toast.LONG);

                        });
                    });

            }).catch(error => {
            if (error.response.status === 401) {
                Toast.show('Usuario o contraseña incorrecto.', Toast.LONG);

            }else{
                Toast.show('Ocurrió un error al iniciar sesión.', Toast.LONG);
            }

            console.log(error.response);
            this.props.service_error();
        });


    }

    render() {
        return (
            <Container style={{backgroundColor:'#fff'}}>
                <Content style={{backgroundColor:'#fff'}}>
                    <View style={{height: 250}}>
                        <Image
                            style={styles.image}
                            source={require('../assets/img/login/login_img.png')}/>
                    </View>
                    <View style={{flex: 1}}>
                        <Grid style={[styles.container,{ backgroundColor:'#fff'}]}>
                            <Row style={{height: 60}}>
                                <Item style={styles.input}>
                                    <Icon active name='mail'/>
                                    <Input placeholder='E-mail'
                                           autoCapitalize='none'
                                           autoCorrect={false}
                                           keyboardType='email-address'
                                           value={this.state.username}
                                           onChangeText={(text) => this.setState({username: text})}/>
                                </Item>
                            </Row>
                            <Row style={{height: 60}}>
                                <Item style={styles.input}>
                                    <Icon active name='lock'/>
                                    <Input placeholder='Contraseña'
                                           autoCapitalize='none'
                                           autoCorrect={false}
                                           secureTextEntry={true}
                                           value={this.state.password}
                                           onChangeText={(text) => this.setState({password: text})}/>
                                </Item>
                            </Row>
                            <Row style={{height: 100, marginTop: 20}}>
                                <Button rounded light style={styles.loginButton} disabled={this.props.isLoading}
                                        onPress={ () => this.userLogin() }>
                                    <H3 style={[styles.btnText, {height: 30}]}>Entrar</H3>
                                </Button>
                            </Row>
                        </Grid>
                    </View>
                </Content>
                <Footer>
                    <Button style={styles.tabButton} light onPress={ this.props.register_try }><H3
                        style={[styles.tabText, {
                            color: "#665b6f",
                            fontFamily: "Quicksand-Regular",
                            height: 30
                        }]}>Registrarme</H3></Button>
                    <Button style={styles.tabButton} light><H3
                        style={[styles.tabText, {fontFamily: "Quicksand-Regular",
                            height: 30}]}>Iniciar Sesión</H3></Button>
                </Footer>
                {(this.props.isLoading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
            </Container>

        );
    }


}
;
const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.LoginReducer.isLoggedIn,
        isLoading: state.LoginReducer.isLoading
    };
};
const mapDispatchToProps = {
    login,
    login_try,
    register_try,
    service_error,
    service_pending,
    service_success
};

const Login = connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

export default Login;
