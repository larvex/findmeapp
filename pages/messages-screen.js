import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    ImageBackground,
    Text,
    ScrollView,
    ListView,
    TouchableOpacity,
    BackHandler,
    Platform, ActivityIndicator
} from 'react-native';
import {
    Button,
    H1,
    H2,
    H3,
    Container,
    Item,
    Icon,
    Body,
    Input,
    Thumbnail,
    List,
    ListItem,
    Header,
    Left,
    Right,
    Title, Content
} from 'native-base';
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import Auth from '../utils/auth';
import {service_success, service_error, service_pending} from "../actions/actionCreator";
import Pusher from 'pusher-js/react-native';

const styles = require("./../assets/styles/styles");

class MessagesScreen extends Component {

    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            user: props.user_data,
            loading: true,
            listViewData: [],
        };
        this.channel = null;
        this.pusher = null;
    }

    static navigationOptions = {
        header: null
    };
    navigate = (routeName, params = {}) => {
        const nav = NavigationActions.navigate({
            routeName,
            params
        });
        this.props.navigation.dispatch(nav);
    };

    deleteRow(secId, rowId, rowMap) {
        rowMap[`${secId}${rowId}`].props.closeRow();
        const newData = [...this.state.listViewData];
        newData.splice(rowId, 1);

        this.setState({listViewData: newData});
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        this.loadList();
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;
        let bearer = this.state.user.bearer;

        this.pusher = new Pusher('44d1bf3056622e4bda9a', {
            activityTimeout:60000,
            cluster: 'us2',
            encrypted: true,
            authEndpoint: Auth.url+'/api/pusher/auth',
            auth: {
                headers: {
                    'Authorization': "Bearer "+bearer
                }
            }
        });

        this.channel = this.pusher.subscribe('private-notification-user.' + this.state.user.id);
        this.channel.bind('new_notif', (data) => {
            console.log(data);
            //this.pushRow(data);
        });


    }

    componentWillUnmount() {
        this.channel.unbind('new_notif');
        this.pusher.unsubscribe('private-notification-user.' + this.state.user.id);
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);

    }

    pushRow = (data) => {

        let listData = this.state.listViewData;
        listData.unshift(data);

        this.setState({listViewData: listData});
    }

    loadList() {

        this.props.service_pending();
        let data = {};
        Auth.getChatList(this.state.user.bearer).then((resp) => {
            let chats = resp.data.map((item,i) =>{
                return {
                    ...item,
                    target: (item.user_1.id === this.state.user.id)?{...item.user_2}:{...item.user_1}
                }
            });
            console.log(chats);
            this.setState({
                listViewData: chats,
                loading: false
            });
            this.props.service_success({});


        }).catch(error => {
            console.log(error.response);
            this.props.service_error();
        });
    }

    renderList() {
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        return (
            <ScrollView style={styles.containerNav}>
                <List
                    dataSource={ds.cloneWithRows(this.state.listViewData)}
                    renderRow={data =>
                        <ListItem style={[styles.navSectionStyle, styles.sectionBottomDash]} onPress={_ => {
                            this.navigate('Chat',{sender_id:data.target.id,sender_name:data.target.name});
                        }}>
                            <Thumbnail medium source={{uri: Auth.url + '/storage/images/' + data.target.user_data.avatar}}/>
                            <Text style={[styles.navItemStyle, {fontSize: 20, width: 250}]}>
                                {data.target.name+' '+data.target.lastname}
                            </Text>
                        </ListItem>}
                    renderRightHiddenRow={(data, secId, rowId, rowMap) =>
                        <Button full light style={{backgroundColor: "#5d022f", width: 80}}
                                onPress={_ => {
                                }}>
                            <Icon style={{color: "#fff"}} active name="trash"/>
                        </Button>
                    }
                    disableRightSwipe={true}
                    disableLeftSwipe={true}
                />
            </ScrollView>
        );

    }

    renderEmptyList() {
        return (
            (!this.props.isLoading) && <View style={styles.containerTransparent}>
                <H3 style={[styles.mediumFont, {margin: 30, textAlign: 'center', color:'#fff'}]}>¡No tienes conversaciones activas!</H3>
            </View>
        );

    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };

    render() {
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        const {navigate} = this.props.navigation;
        const {params} = this.props.navigation.state;
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        {(Platform.OS !== 'android') &&
                        <Button transparent onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                            <Icon name='arrow-back' style={{color: '#fff'}}/>
                        </Button>}

                    </Left>
                    <Body>
                    <Title style={{fontFamily: "Quicksand-Medium", width: 200, color: "#fff"}}>Conversaciones</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content style={styles.containerNav}>
                    {Object.keys(this.state.listViewData).length ? this.renderList() : this.renderEmptyList()}
                </Content>
                {(this.props.isLoading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading: state.LoginReducer.isLoading,
        user_data: state.LoginReducer.data
    };
};
const mapDispatchToProps = {
    service_error,
    service_pending,
    service_success
};
const Messages = connect(mapStateToProps, mapDispatchToProps)(MessagesScreen);

export default Messages;