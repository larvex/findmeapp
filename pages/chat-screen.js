import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    ImageBackground,
    Text,
    ScrollView,
    ListView,
    TouchableOpacity,
    BackHandler,
    Platform, ActivityIndicator,
    Keyboard
} from 'react-native';
import {
    Button,
    H1,
    H2,
    H3,
    Container,
    Item,
    Icon,
    Body,
    Input,
    Thumbnail,
    List,
    ListItem,
    Header,
    Left,
    Right,
    Title, Content, Footer
} from 'native-base';
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import Auth from '../utils/auth';
import {service_success, service_error, service_pending} from "../actions/actionCreator";
import Chatkit from "pusher-chatkit-client/react-native";
import {GiftedChat, Actions, SystemMessage, Send, Bubble} from 'react-native-gifted-chat'
import Toast from 'react-native-simple-toast';

const styles = require("./../assets/styles/styles");

class ChatScreen extends Component {

    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        let uAr = [props.navigation.state.params.sender_id, props.user_data.id].sort();
        console.log(uAr);
        this.scrollViewI = null;
        this.state = {
            user: props.user_data,
            loading: true,
            listViewData: [],
            sender_id: props.navigation.state.params.sender_id,
            sender_name: props.navigation.state.params.sender_name,
            room_id: null,
            room_name: "chat_room." + uAr[0] + '.' + uAr[1],
            room_obj: null,
            current_user: null,
            messages: [],
            firstLoad: true,
            lastdId: null
        };
        this.chatManager=null;
    }

    static navigationOptions = {
        header: null
    };
    navigate = (routeName) => {
        const nav = NavigationActions.navigate({
            routeName
        });
        this.props.navigation.dispatch(nav);
    };

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

        this.props.service_pending();
        // Enable pusher logging - don't include this in production
        Chatkit.logToConsole = true;
        let bearer = this.state.user.bearer;

        const tokenProvider = new Chatkit.TokenProvider({
            url: Auth.url + '/api/pusher/auth',
            authContext: {
                queryParams: {
                    token_provider: "true"
                },
                headers: {
                    'Authorization': "Bearer " + bearer
                }
            }
        });

        this.chatManager = new Chatkit.ChatManager({
            instanceLocator: 'v1:us1:a70b6f8b-fcba-46ce-bcbf-01dcbd754cb6',
            userId: "user_id." + this.state.user.id,
            tokenProvider: tokenProvider

        });

        this.chatManager.connect({
            onSuccess: (currentUser) => {
                console.log("Successful connection");
                Auth.getChatRoom({
                    room_name: this.state.room_name,
                    target: this.state.sender_id
                }, this.state.user.bearer).then((resp) => {
                    console.log('Created private room', resp.data);
                    this.setState({
                            room_id: resp.data.room_id,
                            room_name: resp.data.room_name,
                            room_obj: resp.data,
                            current_user: currentUser
                        }, () => {
                            currentUser.fetchMessagesFromRoom(
                                {
                                    ...this.state.room_obj,
                                    id: this.state.room_obj.room_id,
                                    name: this.state.room_obj.room_name
                                },
                                {},
                                (messages) => {
                                    this.state.current_user.subscribeToRoom(
                                        {
                                            ...this.state.room_obj,
                                            id: this.state.room_obj.room_id,
                                            name: this.state.room_obj.room_name
                                        },
                                        {
                                            newMessage: (message) => {

                                                console.log('Received new message: ', message);
                                                this.pushRow(message);
                                            }
                                        }, 0
                                    );

                                    let newMsgs = messages.map((item, id) => {
                                        return {
                                            _id: item.id,
                                            text: item.text,
                                            createdAt: item.createdAt,
                                            user: {
                                                _id: item.sender.id,
                                                name: item.sender.name,
                                                avatar: item.sender.avatarURL,
                                            }
                                            // Any additional custom parameters are passed through
                                        }

                                    });
                                    newMsgs.reverse();
                                    this.setState(previousState => ({
                                            messages: GiftedChat.append(previousState.messages, newMsgs),
                                            firstLoad: false
                                        }), () => {
                                            this.props.service_success()
                                        }
                                    );

                                    // do something with the messages
                                },
                                (error) => {
                                    console.log(`Error fetching messages from ${myRoom.name}: ${error}`)
                                }
                            );

                        }
                    );


                }).catch(error => {
                    console.log('api error ', error.response);
                    Toast.show('Ocurrió un error al abrir la conversación.', Toast.LONG);
                    this.props.service_error();
                    this.props.navigation.dispatch(NavigationActions.back());

                });
            },
            onError: (error) => {
                console.log("Error on connection", error);
            }
        });

    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
        //this.chatManager.disconnect();
    }

    pushRow = (message) => {
        messages = [
            {
                _id: message.id,
                text: message.text,
                createdAt: message.createdAt,
                user: {
                    _id: message.sender.id,
                    name: message.sender.name,
                    avatar: message.sender.avatarURL,
                }
                // Any additional custom parameters are passed through
            }
        ];


        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }));
    };

    onSend(messages = []) {
        Keyboard.dismiss();
        this.state.current_user.sendMessage(
            {
                text: messages[0].text,
                roomId: this.state.room_id
            },
            (messageId) => {
                /*this.pushRow({
                 ...messages[0],
                 id: messageId,
                 sender: {
                 ...messages[0].user,
                 avatarURL: messages[0].user.avatar,
                 }
                 });*/
                console.log('enviado');
            },
            (error) => {
                console.log('Error', error);
            }
        );
    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };

    render() {
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        const {navigate} = this.props.navigation;
        const {params} = this.props.navigation.state;
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        {(Platform.OS !== 'android') &&
                        <Button transparent onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                            <Icon name='arrow-back' style={{color: '#fff'}}/>
                        </Button>}

                    </Left>
                    <Body>
                    <Title style={{
                        fontFamily: "Quicksand-Medium",
                        width: 200,
                        color: "#fff"
                    }}>{this.state.sender_name}</Title>
                    </Body>
                    <Right/>
                </Header>
                <GiftedChat
                    placeholder='Escribe un mensaje...'
                    messages={this.state.messages}
                    onSend={messages => this.onSend(messages)}
                    user={{
                        _id: 'user_id.' + this.state.user.id,
                        name: this.state.user.name,
                        avatar: Auth.url + '/storage/images/' + this.state.user.user_data.avatar
                    }}
                    renderSend={(props) => {
                        return (
                            <Send
                                {...props}
                            >
                                <View style={{marginRight: 10, marginBottom: 5}}>
                                    <Icon style={{color: "#333"}} active name="send"/>
                                </View>

                            </Send>
                        );
                    }}
                    renderBubble={(props) => {
                        return (
                            <Bubble
                                {...props}
                                wrapperStyle={{
                                    right: {
                                        backgroundColor: '#7b0a34'
                                    }
                                }}
                            />
                        )
                    }}
                    loadEarlier={true}
                />
                {(this.props.isLoading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading: state.LoginReducer.isLoading,
        user_data: state.LoginReducer.data
    };
};
const mapDispatchToProps = {
    service_error,
    service_pending,
    service_success
};
const Chat = connect(mapStateToProps, mapDispatchToProps)(ChatScreen);

export default Chat;