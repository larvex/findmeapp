import React, {Component} from 'react';
import moment from 'moment/min/moment-with-locales';
import Moment from 'react-moment';
import {
    StyleSheet,
    View,
    Image,
    ImageBackground,
    Text,
    ScrollView,
    ListView,
    TouchableOpacity,
    BackHandler,
    Platform,
    ActivityIndicator,
    TouchableWithoutFeedback
} from 'react-native';
import {
    Button,
    H1,
    H2,
    H3,
    Container,
    Item,
    Icon,
    Body,
    Input,
    Thumbnail,
    List,
    ListItem,
    Header,
    Left,
    Right,
    Title,
    Picker,
    Content,
    Footer, Label
} from 'native-base';
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import {service_success, service_error, service_pending} from "../actions/actionCreator";
import Auth from '../utils/auth';


Moment.globalMoment = moment;
Moment.globalLocale = 'es';

const styles = require("./../assets/styles/styles");

class FindHomeChoiceScreen extends Component {

    constructor(props) {
        super(props);
        //this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            user: props.user_data,
            dog: {
                sex: '0',
                breed: '0',
            }
        };
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };
    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel: 'Find Home',
            // Note: By default the icon is only shown on iOS. Search the showIcon option below.
            tabBarIcon: ({tintColor}) => (
                <Icon name="home" style={{color: tintColor}}/>
            ),
            header: null,
            //tabBarOnPress: (data) => navigation.state.params.tabBarPressCb(data, navigation.state.params._this),
        }
    };

    navigate = (routeName, params = {}) => {
        const nav = NavigationActions.navigate({
            routeName,
            params
        });
        this.props.navigation.dispatch(nav);
    };

    onSexChange(value: string) {
        this.setState({
            dog: {
                ...this.state.dog,
                sex: value
            }
        });
    }

    onBreedChange(value: string) {
        this.setState({
            dog: {
                ...this.state.dog,
                breed: value
            }
        });
    }

    getBreeds() {
        const {breeds} = this.state.user;
        let map = breeds.map((item, i) => {
            return (
                <Picker.Item key={item.id} label={item.name} value={item.id}/>
            );
        });
        map.unshift(<Picker.Item key='0' label='Todos' value='0'/>);
        return map;
    }

    render() {
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        {(Platform.OS !== 'android') &&
                        <Button transparent onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                            <Icon name='arrow-back' style={{color: '#fff'}}/>
                        </Button>}
                    </Left>
                    <Body>
                    <Title style={{fontFamily: "Quicksand-Medium", width: 200, color: "#fff"}}>Find Home</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content  contentContainerStyle={{ flex: 1 }}>
                    <View style={{justifyContent:'center', alignItems:'center', padding:30, flex:1}}>
                        <Text style={{fontFamily: "Quicksand-Medium", fontSize:20}}>Filtrar por raza y sexo:</Text>
                        <Item style={[styles.input, {width: '100%', paddingTop:20}]}>
                            <Label>
                                Raza:
                            </Label>
                            <Picker
                                style={{
                                    width:'100%'
                                }}
                                mode="dropdown"
                                placeholder="Raza"
                                selectedValue={this.state.dog.breed}
                                onValueChange={this.onBreedChange.bind(this)}
                            >
                                {this.getBreeds()}
                            </Picker>
                        </Item>
                        <Item style={[styles.input, {width: '100%'}]}>
                            <Label>
                                Sexo:
                            </Label>
                            <Picker
                                style={{
                                    width:'100%'
                                }}
                                mode="dropdown"
                                placeholder="Sexo"
                                selectedValue={this.state.dog.sex}
                                onValueChange={this.onSexChange.bind(this)}
                            >
                                <Item label="Todos" value="0"/>
                                <Item label="Hembra" value="1"/>
                                <Item label="Macho" value="2"/>
                            </Picker>
                        </Item>
                    </View>
                </Content>
                <Footer>
                    <Button disabled={this.props.isLoading} light style={styles.galleryBtn}
                            onPress={() => this.navigate('FindHomeResults', {dog: {...this.state.dog}})}>
                        <H3 style={[styles.mediumFont, {color: "#fff", height: 30}]}>Buscar</H3>
                    </Button>
                </Footer>
                {(this.props.isLoading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading: state.LoginReducer.isLoading,
        user_data: state.LoginReducer.data
    };
};
const mapDispatchToProps = {
    service_error,
    service_pending,
    service_success
};
const FindHomeChoice = connect(mapStateToProps, mapDispatchToProps)(FindHomeChoiceScreen);

export default FindHomeChoice;