import React, {Component} from 'react';
import moment from 'moment/min/moment-with-locales';
import Moment from 'react-moment';
import {Col, Row, Grid} from "react-native-easy-grid";
import {
    StyleSheet,
    View,
    Image,
    ImageBackground,
    Text,
    ScrollView,
    ListView,
    TouchableOpacity,
    BackHandler,
    Platform,
    ActivityIndicator,
    TouchableWithoutFeedback
} from 'react-native';
import {
    Button,
    H1,
    H2,
    H3,
    Container,
    Item,
    Icon,
    Body,
    Input,
    Thumbnail,
    List,
    ListItem,
    Header,
    Left,
    Right,
    Title,
    Picker,
    Content,
    Footer
} from 'native-base';
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import {service_success, service_error, service_pending} from "../actions/actionCreator";
import Auth from '../utils/auth';
import AwesomeAlert from 'react-native-awesome-alerts';
import Modal from "react-native-modal";


Moment.globalMoment = moment;
Moment.globalLocale = 'es';

const styles = require("./../assets/styles/styles");

class FindLoveChoiceScreen extends Component {

    constructor(props) {
        super(props);
        //this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            basic: true,
            listViewData: props.user_data.pets,
            user: props.user_data,
            showAlert: false,
            showModal: false,
            alertMsg: '',
            alertTitle: '',
            dog: {
                sex: "1",
                breed: 1,
            },
            enabled: true
        };
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };
    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel: 'Find Love',
            // Note: By default the icon is only shown on iOS. Search the showIcon option below.
            tabBarIcon: ({tintColor}) => (
                <Icon name="heart" style={{color: tintColor}}/>
            ),
            header: null,
            //tabBarOnPress: (data) => navigation.state.params.tabBarPressCb(data, navigation.state.params._this),
        }
    };

    navigate = (routeName, params = {}) => {
        const nav = NavigationActions.navigate({
            routeName,
            params
        });
        this.props.navigation.dispatch(nav);
    };

    onSexChange(value: string) {
        this.setState({
            dog: {
                ...this.state.dog,
                sex: value
            }
        });
    }

    onBreedChange(value: string) {
        this.setState({
            dog: {
                ...this.state.dog,
                breed: value
            }
        });
    }

    getBreeds() {
        const {breeds} = this.state.user;
        return breeds.map((item, i) => {
            return (
                <Picker.Item key={item.id} label={item.name} value={item.id}/>
            );
        });
    }

    showAlert = (msg, title, secId, rowId, rowMap, id) => {

        //this.deleteRow(secId, rowId, rowMap, data.id)

        this.setState({
            showAlert: true,
            alertMsg: msg,
            alertTitle: title,
            drop: {secId, rowId, rowMap, id}
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false,
            drop: false
        });
    };
    hideModal = () => {
        this.setState({
            showModal: false,
        }, this.navigate('FindLoveResults', {dog: {...this.state.dog, sex: (this.state.dog.sex === '1' ? '2' : '1')}}));
    };

    renderList() {
        //const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        return (
            <ScrollView>
                <List
                    dataArray={this.state.listViewData}
                    //dataSource={ds.cloneWithRows(this.state.listViewData)}
                    renderRow={data =>
                        <ListItem style={[styles.listItemPets, {marginLeft: 0}]}
                                  onPress={_ => this.navigate('FindLoveResults', {dog: {...data}})}>
                            <Thumbnail large square source={{uri: Auth.url + '/storage/images/' + data.avatar}}/>
                            <H1 style={[styles.mediumFont, {paddingLeft: 10}]}>{data.name}</H1>
                            <Moment element={Text} fromNow ago
                                    style={[styles.smallFont, {color: '#333'}]}>{data.birthday}</Moment>
                            <Text style={styles.smallFont}>{this.state.user.breeds[data.breed - 1].name}</Text>
                        </ListItem>}
                />
            </ScrollView>
        );

    }

    renderEmptyList() {
        return (
            <View style={styles.containerTransparent}>
                <H3 style={[styles.mediumFont, {margin: 30, textAlign: 'center'}]}>¡No tienes mascotas aún, agrega tu
                    primera mascota ahora!</H3>
            </View>
        );

    }

    render() {
        const {navigate} = this.props.navigation;
        const {params} = this.props.navigation.state;
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        {(Platform.OS !== 'android') &&
                        <Button transparent onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                            <Icon name='arrow-back' style={{color: '#fff'}}/>
                        </Button>}
                    </Left>
                    <Body>
                    <Title style={{fontFamily: "Quicksand-Medium", width: 200, color: "#fff"}}>Elige una Mascota</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content  contentContainerStyle={{ flex: 1 }}>
                    {Object.keys(this.state.listViewData).length ? this.renderList() : this.renderEmptyList()}
                </Content>
                <Footer>
                    <Button disabled={this.props.isLoading} light style={styles.galleryBtn}
                            onPress={() => this.setState({
                                showModal: true
                            })}>
                        <H3 style={[styles.mediumFont, {color: "#fff", height: 30}]}>Ó elige raza y sexo</H3>
                    </Button>
                </Footer>
                {(this.props.isLoading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title={this.state.alertTitle}
                    message={this.state.alertMsg}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={false}
                    showConfirmButton={true}
                    cancelText="Cancelar"
                    confirmText="Entendido"
                    confirmButtonColor="#7b0a34"
                    onCancelPressed={() => {
                        this.hideAlert();
                    }}
                    onConfirmPressed={() => {
                        this.hideAlert();
                    }}
                />
                <Modal
                    isVisible={this.state.showModal}
                    onBackButtonPress={()=>this.setState({
                        showModal: false,
                    })}
                    onBackdropPress={()=>this.setState({
                        showModal: false,
                    })}
                >
                    <View style={{backgroundColor: '#fff', borderRadius: 15, alignItems: 'center'}}>
                        <View style={{paddingLeft: 30, paddingRight: 30, paddingTop: 5, paddingBottom: 5}}>
                            <Text style={{
                                textAlign: 'center',
                                fontFamily: "Quicksand-Medium",
                                color: "#333",
                                fontSize: 22,
                                paddingRight: 15
                            }}>Elige raza y sexo
                            </Text>
                        </View>
                        <View style={{width: '100%', borderBottomWidth: 1, borderBottomColor: '#ccc'}}/>
                        <View style={{paddingLeft: 30, paddingRight: 30, paddingTop: 5, paddingBottom: 5}}>
                            <Item style={[styles.input, {width: '100%'}]}>
                                <Picker
                                    style={{
                                        ...Platform.select({
                                            android: {
                                                width: '50%',
                                            },
                                            ios: {
                                                width: 100
                                            }
                                        })
                                    }}
                                    mode="dropdown"
                                    placeholder="Raza"
                                    selectedValue={this.state.dog.breed}
                                    onValueChange={this.onBreedChange.bind(this)}
                                >
                                    {this.getBreeds()}
                                </Picker>
                                <Picker
                                    style={{
                                        ...Platform.select({
                                            android: {
                                                width: '50%',
                                            }
                                            ,
                                            ios: {
                                                width: 100
                                            }
                                        })
                                    }}
                                    mode="dropdown"
                                    placeholder="Sexo"
                                    selectedValue={this.state.dog.sex}
                                    onValueChange={this.onSexChange.bind(this)}
                                >
                                    <Item label="Hembra" value="1"/>
                                    <Item label="Macho" value="2"/>
                                </Picker>
                            </Item>
                        </View>
                        <Button light style={[styles.addPetButton, {
                            width: '100%',
                            borderBottomLeftRadius: 15,
                            borderBottomRightRadius: 15,
                            height: 50
                        }]}
                                onPress={() => this.hideModal()}>
                            <H3 style={[styles.mediumFont, {color: "#fff", height: 25}]}>Buscar</H3>
                        </Button>
                    </View>
                </Modal>
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading: state.LoginReducer.isLoading,
        user_data: state.LoginReducer.data
    };
};
const mapDispatchToProps = {
    service_error,
    service_pending,
    service_success
};
const FindLoveChoice = connect(mapStateToProps, mapDispatchToProps)(FindLoveChoiceScreen);

export default FindLoveChoice;