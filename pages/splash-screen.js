import React, {Component} from 'react';
import {StyleSheet, View, Image, ImageBackground, Text} from 'react-native';
import {Button, H1, H2, H3, Container, Item, Icon, Input, Thumbnail, Body,
    Header,
    Left,
    Right,
    Title} from 'native-base';


const styles = require("./../assets/styles/styles");

export default class SplashScreen extends Component {
    render() {
        return (
            <Container>
                <ImageBackground
                    style={{
                        flex: 1,
                        justifyContent:'center',
                        alignItems:'center',
                    }}
                    source={require('../assets/img/splash/splash.png')}>
                    <Image
                        source={require('../assets/img/splash/logo_splash.png')}/>
                </ImageBackground>
            </Container>
        );
    }
}
