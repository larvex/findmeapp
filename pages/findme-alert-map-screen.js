import React, {Component} from 'react';
import {
    StyleSheet, View, Image, ImageBackground, Text, KeyboardAvoidingView,
    AppRegistry,
    Dimensions,
    TouchableHighlight, Alert, BackHandler, Platform, TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import {
    Button,
    H1,
    H2,
    H3,
    Container,
    Item,
    Icon,
    Input,
    Thumbnail,
    Body,
    Header,
    Left,
    Right,
    Title,
    Content, Picker, InputGroup, Footer
} from 'native-base';
import {Col, Row, Grid} from "react-native-easy-grid";
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import {service_success, service_error, service_pending} from "../actions/actionCreator";
import Auth from '../utils/auth';
import MapView from 'react-native-maps';
import Permissions from 'react-native-permissions';
import Toast from 'react-native-simple-toast';

const styles = require("./../assets/styles/styles");
class FoundPetScreen extends Component {

    constructor(props) {
        super(props);
        props._mounted = false;
        this.state = {
            user: props.user_data,
            showAlert: false,
            alertMsg: '',
            alertTitle: '',
            description: '',
            region: {
                latitude: 19.432608,
                longitude: -99.133209,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
                done: false,
                error: false
            },
            marker: {
                coordinate: {
                    latitude: 0,
                    longitude: 0,
                },
                rendered: false
            }

        };
    }

    static navigationOptions = {
        header: null
    };
    navigate = (routeName) => {
        const nav = NavigationActions.navigate({
            routeName
        });
        this.props.navigation.dispatch(nav);
    };

    getDeltas(lat, lon, distance) {
        distance = distance / 2
        const circumference = 40075
        const oneDegreeOfLatitudeInMeters = 111.32 * 1000
        const angularDistance = distance / circumference

        const latitudeDelta = distance / oneDegreeOfLatitudeInMeters
        const longitudeDelta = Math.abs(Math.atan2(
            Math.sin(angularDistance) * Math.cos(lat),
            Math.cos(angularDistance) - Math.sin(lat) * Math.sin(lat)))

        return {
            latitude: lat,
            longitude: lon,
            latitudeDelta,
            longitudeDelta,
            done: true,
            error: false
        };
    }

    componentDidMount() {
        this.props._mounted = true;
        console.log('DELTAS');

        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

        Permissions.request('location').then(response => {
            // Returns once the user has chosen to 'allow' or to 'not allow' access
            // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
            this.setState({ locationPermission: response },
                ()=>{
                    navigator.geolocation.getCurrentPosition(
                        (position) => {
                            let region = this.getDeltas(
                                position.coords.latitude,
                                position.coords.longitude,
                                80
                            );

                            console.log(region);

                            this.setState({
                                region
                            });
                        },
                        (error) => {
                            Toast.show('Activa la ubicación del dispositivo para poder determinar donde te encuentras', Toast.LONG);

                            console.log(error);

                            if(true){
                                this.setState({region: {
                                    latitude: 19.432608,
                                    longitude: -99.133209,
                                    latitudeDelta: 0.005,
                                    longitudeDelta: 0.005,
                                    error: error,
                                    done: true}})
                            }

                        },
                        //{enableHighAccuracy: true, timeout: 15000, maximumAge: 1000},
                    )
                }

            )
        });
    }

    componentWillUnmount() {
        this.props._mounted = false;
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };

    onRegionChange(region) {
        this.setState({
            region: {
                ...region,
                done: true,
                error: false
            }
        });
    }

    onPress(press) {
        this.setState({
            marker: {
                rendered: true,
                coordinate: press.coordinate
            }
        });
    }

    savePos(){
        this.props.navigation.state.params.setLocation({
            latitude:this.state.marker.coordinate.latitude,
            longitude:this.state.marker.coordinate.longitude
        });
        this.props.navigation.dispatch(NavigationActions.back());
    }


    render() {
        return (
            <Container>
                {(Platform.OS !== 'android') &&<View searchBar style={{backgroundColor:'#202020',flexDirection: 'row', padding:10}}>
                    {(Platform.OS !== 'android') &&
                    <Button transparent iconLeft  style={{height:35}} onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                        <Icon name='arrow-back' style={{color: '#fff'}}/>
                    </Button>}
                    {(false)&&<InputGroup rounded style={{flex:1, marginLeft:10, marginRight:10, backgroundColor:'#fff',height:35, paddingLeft:10, paddingRight:10}}>
                        <Icon name="ios-search"/>
                        <Input placeholder="Buscar ubicación"/>
                    </InputGroup>&&
                    <Button transparent  style={{height:35}}>
                        <Text style={{fontFamily: "Quicksand-Small", color: "#fff"}}>Buscar</Text>
                    </Button>}
                </View>}
                {(this.state.region.error) && <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                    <Text>Error</Text>
                </View>}
                {(!this.state.region.done) && <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                    <Text>Obteniendo ubicación...</Text>
                </View>}
                {(this.state.region.done) &&
                <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                    <MapView
                        style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0
                        }}
                        region={this.state.region}
                        onRegionChangeComplete={ (region) => this.onRegionChange(region)}
                        onPress={ e => this.onPress(e.nativeEvent) }
                    >
                        {(this.state.marker.rendered) && <MapView.Marker
                            coordinate={this.state.marker.coordinate}
                        />}
                        {(this.state.marker.rendered) && <MapView.Circle
                            key={(this.state.marker.coordinate.longitude + this.state.marker.coordinate.latitude).toString()}
                            center={this.state.marker.coordinate}
                            radius={100}
                            strokeColor="#7b0a34"
                            fillColor="rgba(220,61,64,0.2)"
                        />}
                    </MapView>
                    <Text style={{
                        backgroundColor: 'rgba(52,52,52,0.7)',
                        position: 'absolute',
                        top: 0,
                        padding: 10,
                        color: '#fff',
                        fontFamily: "Quicksand-Medium",
                        fontSize: 14,
                    }}>
                        Indica la ubicación aproximada del incidente dando click sobre el mapa.
                    </Text>
                </View>}
                <Footer>
                    <Button disabled={this.props.isLoading} rounded light style={styles.galleryBtn} onPress={() => this.savePos()}>
                        <H3 style={[styles.mediumFont, {color: "#fff", height: 30}]}>Listo</H3>
                    </Button>
                </Footer>

                {(this.props.isLoading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading: state.LoginReducer.isLoading,
        user_data: state.LoginReducer.data
    };
};
const mapDispatchToProps = {
    service_error,
    service_pending,
    service_success
};
const FoundPet = connect(mapStateToProps, mapDispatchToProps)(FoundPetScreen);

export default FoundPet;
