import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    ImageBackground,
    Text,
    KeyboardAvoidingView,
    TouchableOpacity,
    Platform,
    BackHandler,
    ActivityIndicator
} from 'react-native';
import {
    Button,
    H1,
    H2,
    H3,
    Container,
    Item,
    Icon,
    Input,
    Thumbnail,
    Body,
    Header,
    Left,
    Right,
    Title,
    Content,
    Picker,
    DeckSwiper,
    Card,
    CardItem
} from 'native-base';
import {Col, Row, Grid} from "react-native-easy-grid";
import {service_success, service_error, service_pending} from "../actions/actionCreator";
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import Auth from '../utils/auth';
import Toast from 'react-native-simple-toast';

const styles = require("./../assets/styles/styles");

class HomeListScreen extends Component {
    constructor(props) {
        super(props);
        let dog = props.navigation.state.params.dog;
        console.log(dog);
        this.state = {
            dog: {
                ...dog
            },
            user: props.user_data,
            list: [
                {
                    bio: '',
                    name: '',
                    avatar: require('./../assets/img/tinder/tinderdog1.png'),
                },
            ],
            loading: true
        };
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        this.loadList();
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };

    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel: 'Find Love',
            // Note: By default the icon is only shown on iOS. Search the showIcon option below.
            tabBarIcon: ({tintColor}) => (
                <Icon name="heart" style={{color: tintColor}}/>
            ),
            header: null,
            //tabBarOnPress: (data) => navigation.state.params.tabBarPressCb(data, navigation.state.params._this),
        }
    };

    loadList() {

        this.props.service_pending();
        let data = {};
        Auth.findHomeList({
            breed: this.state.dog.breed,
            sex: this.state.dog.sex
        }, this.state.user.bearer).then((resp) => {
            console.log(resp.data);
            this.setState({
                list: resp.data,
                loading: false
            });
            this.props.service_success({});


        }).catch(error => {
            console.log(error.response);
            this.props.service_error();
        });
    }

    navigate = (routeName, params = {}) => {
        const nav = NavigationActions.navigate({
            routeName,
            params
        });
        this.props.navigation.dispatch(nav);
    };

    render() {
        const {navigate} = this.props.navigation;
        const {params} = this.props.navigation.state;
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        {(Platform.OS !== 'android') &&
                        <Button transparent onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                            <Icon name='arrow-back' style={{color: '#fff'}}/>
                        </Button>}
                    </Left>
                    <Body>
                    <Title style={{fontFamily: "Quicksand-Medium", width: 200, color: "#fff"}}>Find Love</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content contentContainerStyle={{flex: 1}}>
                    {(!this.state.loading && this.state.list.length <= 0) && <View style={styles.containerTransparent}>
                        <H3 style={[styles.mediumFont, {margin: 30, textAlign: 'center'}]}>¡No se encontraron
                            coincidencias en este momento!</H3>
                    </View>}
                    {(!this.state.loading && this.state.list.length > 0) && <DeckSwiper
                        ref={(c) => this._deckSwiper = c}
                        dataSource={this.state.list}
                        renderItem={item =>
                            <Card style={{elevation: 3}}>
                                <CardItem cardBody style={{flexDirection: 'column'}}>
                                    <View style={{width: '100%'}}>
                                        <Image source={{uri: Auth.url + '/storage/images/' + item.avatar}}
                                               style={{flex: 1, margin: 0, height: 320}}/>
                                        <View style={{
                                            position: 'absolute',
                                            bottom: 0,
                                            paddingBottom: 10,
                                            paddingTop: 10,
                                            width: '100%',
                                            backgroundColor: 'rgba(100,0,50,0.5)'
                                        }}>
                                            <Text style={{
                                                textAlign: 'center',
                                                fontFamily: "Quicksand-Medium",
                                                color: "#fff",
                                                fontSize: 30
                                            }}>
                                                {item.name}
                                            </Text>

                                        </View>
                                    </View>
                                    <View style={{
                                        flexDirection: 'row',
                                        width: '100%',
                                        backgroundColor: '#aaa',
                                        justifyContent: 'center'
                                    }}>
                                        <Thumbnail large square
                                                   source={{uri: Auth.url + '/storage/images/' + item.avatar}}/>
                                        <Thumbnail large square
                                                   source={{uri: Auth.url + '/storage/images/' + item.avatar}}/>
                                        <Thumbnail large square
                                                   source={{uri: Auth.url + '/storage/images/' + item.avatar}}/>
                                        <Thumbnail large square
                                                   source={{uri: Auth.url + '/storage/images/' + item.avatar}}/>
                                    </View>
                                </CardItem>
                                <CardItem footer>
                                    <Body>
                                    <H3 note style={[styles.smallFont, {width: 250}]}>{item.bio}</H3>
                                    </Body>
                                    <Right>
                                        <Button transparent onPress={ () => {

                                            if (this._deckSwiper._root.state.selectedItem.sent) {
                                                Toast.show('Ya enviaste una solicitud a esta mascota', Toast.LONG);
                                                return;
                                            }
                                            let r_dog = this.state.dog;
                                            this.props.service_pending();

                                            Auth.sendAdoptionRequest(
                                                item.id,
                                                this.state.user.bearer
                                            ).then((resp) => {
                                                console.log(resp.data);
                                                this.props.service_success({});
                                                this._deckSwiper._root.state.selectedItem.sent = true;
                                                Toast.show('Se envió tu solicitud', Toast.LONG);
                                            }).catch(error => {
                                                console.log(error.response);
                                                this.props.service_error();
                                            });
                                        }
                                        }>
                                            <Icon name="home"
                                                  style={{color: '#ED4A6A', fontSize: 35, marginRight: 10}}/>
                                        </Button>
                                    </Right>
                                </CardItem>
                            </Card>
                        }
                    />}
                </Content>
                {(this.props.isLoading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
            </Container>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        isLoading: state.LoginReducer.isLoading,
        user_data: state.LoginReducer.data
    };
};
const mapDispatchToProps = {
    service_error,
    service_pending,
    service_success
};
const HomeList = connect(mapStateToProps, mapDispatchToProps)(HomeListScreen);

export default HomeList;
