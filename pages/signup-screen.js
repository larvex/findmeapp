import React, {Component} from 'react';
import {connect} from "react-redux";
import {StyleSheet, View, Image, Text, KeyboardAvoidingView, ActivityIndicator} from 'react-native';
import {Button, H1, H2, H3, Container, Item, Icon, Input, Content, Footer} from 'native-base';
import {Col, Row, Grid} from "react-native-easy-grid";
import {login, login_try, register_try, service_success, service_error, service_pending} from "../actions/actionCreator";
import Auth from '../utils/auth';
import Toast from 'react-native-simple-toast';


const styles = require("./../assets/styles/styles");
class SignupScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            username: '',
            password: '',
            password_check: ''
        };
    }

    static navigationOptions = {
        header: null
    };

    userRegister() {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        if (!this.state.password || !this.state.password_check || !this.state.name || !this.state.username) {
            Toast.show('Debes llenar todos los campos.', Toast.LONG);
            return;
        }
        if (this.state.password !== this.state.password_check) {
            Toast.show('Las contraseñas no coinciden.', Toast.LONG);
            return;
        }
        if(reg.test(this.state.username) === false)
        {
            Toast.show('El correo no es válido.', Toast.LONG);
            return;
        }
        this.props.service_pending();

        Auth.register(this.state.username, this.state.name, this.state.password)
            .then((resp) => {
                this.props.service_success();

                if (resp.status === 202) {
                    Toast.show('Este correo ya esta registrado.', Toast.LONG);
                } else {
                    Toast.show('Usuario registrado, ahora puedes iniciar sesión.', Toast.LONG);
                }
                console.log('User registered or failed', resp.data);

                this.props.login_try();

            }).catch(error => {
            if (error.response.status === 401) {
                Toast.show('Ocurrió un error al registrar el usuario.', Toast.LONG);

            } else {
                Toast.show('Ocurrió un error, intenta de nuevo.', Toast.LONG);
            }

            console.log(error.response);
            this.props.service_error();
        });


    }

    render() {
        return (
            <Container style={{backgroundColor:'#fff'}}>
                <Content style={{backgroundColor:'#fff'}}>
                    <View style={{height: 250}}>
                        <Image
                            style={styles.image}
                            source={require('../assets/img/login/login_img.png')}/>
                    </View>
                    <View style={{flex: 1}}>
                        <Grid style={[styles.container,{ backgroundColor:'#fff'}]}>
                            <Row style={{height: 60}}>
                                <Item style={styles.input}>
                                    <Icon active name='person'/>
                                    <Input placeholder='Nombre'
                                           autoCorrect={false}
                                           autoFocus={false}
                                           value={this.state.name}
                                           onChangeText={(text) => this.setState({name: text})}/>
                                </Item>
                            </Row>
                            <Row style={{height: 60}}>
                                <Item style={styles.input}>
                                    <Icon active name='mail'/>
                                    <Input placeholder='E-mail'
                                           autoCapitalize='none'
                                           autoCorrect={false}
                                           autoFocus={false}
                                           keyboardType='email-address'
                                           value={this.state.username}
                                           onChangeText={(text) => this.setState({username: text})}/>
                                </Item>
                            </Row>
                            <Row style={{height: 60}}>
                                <Col>
                                    <Item style={[styles.input, {marginLeft: 10}]}>
                                        <Icon active name='lock'/>
                                        <Input placeholder='Contraseña'
                                               autoCapitalize='none'
                                               autoCorrect={false}
                                               secureTextEntry={true}
                                               value={this.state.password}
                                               onChangeText={(text) => this.setState({password: text})}/>
                                    </Item>
                                </Col>
                                <Col>
                                    <Item style={styles.input}>
                                        <Input placeholder='Confirmar'
                                               autoCapitalize='none'
                                               autoCorrect={false}
                                               secureTextEntry={true}
                                               value={this.state.password_check}
                                               onChangeText={(text) => this.setState({password_check: text})}/>
                                    </Item>
                                </Col>

                            </Row>
                            <Row style={{height: 50, marginTop: 20}}>
                                <Button rounded light style={styles.loginButton} disabled={this.props.isLoading}
                                        onPress={ () => this.userRegister() }>
                                    <H3 style={[styles.btnText, {height: 30}]}>Registrarme</H3>
                                </Button>
                            </Row>
                        </Grid>
                    </View>
                </Content>
                <Footer>
                    <Button style={styles.tabButton} light><H3
                        style={[styles.tabText, {
                            fontFamily: "Quicksand-Regular",
                            height: 30
                        }]}>Registrarme</H3></Button>
                    <Button style={styles.tabButton} light onPress={ this.props.login_try }><H3
                        style={[styles.tabText, {
                            color: "#665b6f",
                            fontFamily: "Quicksand-Regular", height: 30
                        }]}>Iniciar Sesión</H3></Button>
                </Footer>
                {(this.props.isLoading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
            </Container>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.LoginReducer.isLoggedIn,
        isLoading: state.LoginReducer.isLoading
    };
};
const mapDispatchToProps = {
    login,
    login_try,
    register_try,
    service_error,
    service_pending,
    service_success
};

const Signup = connect(mapStateToProps, mapDispatchToProps)(SignupScreen);
export default Signup;
