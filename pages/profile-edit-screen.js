import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    ImageBackground,
    Text,
    BackHandler,
    Platform,
    KeyboardAvoidingView, ActivityIndicator
} from 'react-native';
import {
    Button,
    H1,
    H2,
    H3,
    Container,
    Item,
    Icon,
    Input,
    Thumbnail,
    Header,
    Left,
    Right,
    Title,
    Body, Content, Picker
} from 'native-base';
import {Col, Row, Grid} from "react-native-easy-grid";
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import {service_success, service_error, service_pending} from "../actions/actionCreator";
import Auth from '../utils/auth';
import Toast from 'react-native-simple-toast';
const ItemP = Picker.Item;

const styles = require("./../assets/styles/styles");

class ProfileEditScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: props.user_data
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };

    navigate = (routeName) => {
        const nav = NavigationActions.navigate({
            routeName
        });
        this.props.navigation.dispatch(nav);
    }

    saveProfile() {
        if (this.state.user.user_data.state_id === 0) {
            Toast.show('Debes elegir un estado', Toast.LONG);
            return;
        }
        this.props.service_pending();
        let data = {};


        Auth.saveProfile(this.state.user, this.state.user.bearer).then((resp) => {

            data = resp.data;
            this.props.service_success(data);
            console.log('got data', data);
            this.props.navigation.dispatch(NavigationActions.back());
            Toast.show('Se editó tu información', Toast.LONG);


        }).catch(error => {
            console.log(error.response);
            this.props.service_error();
            Toast.show('Ocurrió un error inesperado.', Toast.LONG);

        });
    }

    getStates() {
        const {states} = this.state.user;
        return states.map((item, i) => {
            return (
                <ItemP key={item.id} label={item.name} value={item.id}/>
            );
        });
    }

    onStateChange(value: string) {
        this.setState({
            user: {
                ...this.state.user,
                user_data: {
                    ...this.state.user.user_data,
                    state_id: value
                }
            }
        });
    }

    render() {
        let picUri = {uri: Auth.url + '/storage/images/' + this.props.user_data.user_data.avatar};

        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        {(Platform.OS !== 'android') &&
                        <Button transparent onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                            <Icon name='arrow-back' style={{color: '#fff'}}/>
                        </Button>}
                    </Left>
                    <Body>
                    <Title style={{fontFamily: "Quicksand-Medium", width: 200, color: "#fff"}}>Editar Perfil</Title>
                    </Body>
                    <Right/>
                </Header>
                <ImageBackground
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                    source={require('../assets/img/profile/profile_back.png')}>
                    <KeyboardAvoidingView style={{flex: 1}}>

                        <Content>
                            <Grid style={styles.containerTransparent}>
                                <Row style={{height: 130}}>
                                    <Col size={40}>
                                        <Thumbnail large style={{
                                            width: 120,
                                            height: 120,
                                            borderRadius: 60,
                                            borderColor: "#7b0a34",
                                            borderWidth: 4,
                                            marginTop: 10,
                                            marginLeft: 10
                                        }} source={picUri}/>
                                        <Button rounded style={styles.changeBtn}
                                                onPress={() => this.navigate('ProfilePicEdit')}>
                                            <Icon style={styles.changeIcon} name='create'/>
                                        </Button>
                                    </Col>
                                    <Col size={60}>
                                        <Row style={{height: 60}}>
                                            <Item style={styles.input}>
                                                <Icon style={{color: "#666"}} active name='person'/>
                                                <Input style={[styles.placeholder, {color: "#fff"}]}
                                                       placeholder='Nombre'
                                                       value={this.state.user.name}
                                                       onChangeText={(text) => this.setState({
                                                           user: {
                                                               ...this.state.user,
                                                               name: text
                                                           }
                                                       })}/>
                                            </Item>
                                        </Row>
                                        <Row style={{height: 60}}>
                                            <Item style={styles.input}>
                                                <Input style={[styles.placeholder, {color: "#fff"}]}
                                                       placeholder='Apellido'
                                                       value={this.state.user.lastname}
                                                       onChangeText={(text) => this.setState({
                                                           user: {
                                                               ...this.state.user,
                                                               lastname: text
                                                           }
                                                       })}/>
                                            </Item>
                                        </Row>

                                    </Col>
                                </Row>
                                <Row style={{height: 60}}>
                                    <Item style={styles.input}>
                                        <Icon style={{color: "#666"}} active name='chatbubbles'/>
                                        <Input style={[styles.placeholder, {color: "#fff"}]} placeholder='WhatsApp'
                                               value={this.state.user.user_data.whatsapp}
                                               onChangeText={(text) => this.setState({
                                                   user: {
                                                       ...this.state.user,
                                                       user_data: {
                                                           ...this.state.user.user_data,
                                                           whatsapp: text
                                                       }
                                                   }
                                               })}/>
                                    </Item>
                                </Row>
                                <Row style={{height: 60}}>

                                    <Col>
                                        <Picker
                                            style={{...Platform.select({
                                                ios: {
                                                },
                                                android: {
                                                    color: '#fff',
                                                },
                                            }),flex: 1, marginLeft: 10, height:60, marginTop:(Platform.OS !== 'android')?30:15, width:'100%'}}
                                            mode="dropdown"
                                            placeholder="Estado"
                                            iosHeader="Estado"
                                            headerBackButtonText="Volver"
                                            selectedValue={this.state.user.user_data.state_id}
                                            onValueChange={this.onStateChange.bind(this)}
                                            itemStyle={{ backgroundColor: '#fff', color:'#fff', marginLeft: 0, paddingLeft: 15}}
                                            itemTextStyle={{ fontSize: 18, color: '#000' }}
                                            textStyle={{color:'#fff'}}
                                        >
                                            {this.getStates()}
                                        </Picker>
                                    </Col>
                                    <Col>
                                        <Item style={styles.input}>
                                            <Input style={[styles.placeholder, {color: "#fff"}]} placeholder='Colonia'
                                                   value={this.state.user.user_data.suburb}
                                                   onChangeText={(text) => this.setState({
                                                       user: {
                                                           ...this.state.user,
                                                           user_data: {
                                                               ...this.state.user.user_data,
                                                               suburb: text
                                                           }
                                                       }
                                                   })}/>
                                        </Item>
                                    </Col>

                                </Row>
                                <Row style={{height: 60}}>

                                    <Col size={2}>
                                        <Item style={styles.input}>
                                            <Input style={[styles.placeholder, {color: "#fff"}]} placeholder='Calle'
                                                   value={this.state.user.user_data.street}
                                                   onChangeText={(text) => this.setState({
                                                       user: {
                                                           ...this.state.user,
                                                           user_data: {
                                                               ...this.state.user.user_data,
                                                               street: text
                                                           }
                                                       }
                                                   })}/>
                                        </Item>
                                    </Col>
                                    <Col size={1}>
                                        <Item style={styles.input}>
                                            <Input style={[styles.placeholder, {color: "#fff"}]} placeholder='#'
                                                   value={this.state.user.user_data.int}
                                                   onChangeText={(text) => this.setState({
                                                       user: {
                                                           ...this.state.user,
                                                           user_data: {
                                                               ...this.state.user.user_data,
                                                               int: text
                                                           }
                                                       }
                                                   })}/>
                                        </Item>
                                    </Col>
                                    <Col size={1}>
                                        <Item style={styles.input}>
                                            <Input style={[styles.placeholder, {color: "#fff"}]} placeholder='C.P.'
                                                   value={this.state.user.user_data.zip}
                                                   onChangeText={(text) => this.setState({
                                                       user: {
                                                           ...this.state.user,
                                                           user_data: {
                                                               ...this.state.user.user_data,
                                                               zip: text
                                                           }
                                                       }
                                                   })}/>
                                        </Item>
                                    </Col>

                                </Row>
                                <Row style={{marginTop: 15, marginBottom: 15}}>
                                    <Button disabled={this.props.isLoading} rounded light style={styles.loginButton} onPress={() => this.saveProfile()}>
                                        <H3 style={styles.btnText}>Guardar</H3>
                                    </Button>
                                </Row>
                                <Row>
                                    <H3 style={[styles.smallFont, {
                                        color: "#ccc",
                                        textAlign: 'left',
                                        alignSelf: 'center'
                                    }]}>Tus
                                        datos jamás serán
                                        compartidos sin autorización previa, tu número telefónico es esencial para poder
                                        ponerte en
                                        contacto con otros usuarios de FindMe en caso de un match, búsqueda o
                                        adopción.</H3>
                                </Row>
                            </Grid>
                            {(this.props.isLoading) && <View style={styles.loader}>
                                <ActivityIndicator size="large"/>
                            </View>}
                        </Content>
                    </KeyboardAvoidingView>

                </ImageBackground>
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading: state.LoginReducer.isLoading,
        user_data: state.LoginReducer.data
    };
};
const mapDispatchToProps = {
    service_error,
    service_pending,
    service_success
};
const ProfileEdit = connect(mapStateToProps, mapDispatchToProps)(ProfileEditScreen);

export default ProfileEdit;