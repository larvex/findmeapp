import React, {Component} from 'react';
import moment from 'moment/min/moment-with-locales';
import Moment from 'react-moment';
import {
    StyleSheet,
    View,
    Image,
    ImageBackground,
    Text,
    ScrollView,
    ListView,
    TouchableOpacity,
    BackHandler,
    Platform,
    ActivityIndicator
} from 'react-native';
import {
    Button,
    H1,
    H2,
    H3,
    Container,
    Item,
    Icon,
    Body,
    Input,
    Thumbnail,
    List,
    ListItem,
    Header,
    Left,
    Right,
    Title
} from 'native-base';
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import {service_success, service_error, service_pending} from "../actions/actionCreator";
import Auth from '../utils/auth';
import AwesomeAlert from 'react-native-awesome-alerts';
import Toast from 'react-native-simple-toast';


Moment.globalMoment = moment;
Moment.globalLocale = 'es';

const styles = require("./../assets/styles/styles");

class FindmeDogsScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            basic: true,
            listViewData: props.user_data.pets,
            user: props.user_data,
            showAlert: false,
            alertMsg: '',
            alertTitle: ''
        };
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };

    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel: 'Mascotas',
            // Note: By default the icon is only shown on iOS. Search the showIcon option below.
            tabBarIcon: ({tintColor}) => (
                <Icon name="paw" style={{color: tintColor}}/>
            ),
            header: null,
            //tabBarOnPress: (data) => navigation.state.params.tabBarPressCb(data, navigation.state.params._this),
        }
    };

    navigate = (routeName, params = {}) => {
        const nav = NavigationActions.navigate({
            routeName,
            params
        });
        this.props.navigation.dispatch(nav);
    };

    showAlert = (msg, title, secId, rowId, rowMap, id) => {

        //this.deleteRow(secId, rowId, rowMap, data.id)

        this.setState({
            showAlert: true,
            alertMsg: msg,
            alertTitle: title,
            drop: {secId, rowId, rowMap, id}
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false,
            drop: false
        });
    };

    renderList() {
        return (
            <ScrollView>
                <H3 style={[styles.mediumFont, {padding: 30, textAlign: 'center'}]}>¿Quién está extraviado?</H3>

                <List
                    dataArray={this.state.listViewData}
                    renderRow={(data, sectionId, rowId, highlightRow) =>
                        <ListItem style={[styles.listItemPets, {marginLeft: 0}]} onPress={_ => {
                            if(!(data.find_me.length > 0)){
                                this.navigate('FindMeAlert', {dog: {...data, rowId}})
                            }else{
                                Toast.show('Esta mascota ya tiene una alerta activada', Toast.LONG);
                            }

                            }
                        }>
                            <Thumbnail large square source={{uri: Auth.url + '/storage/images/' + data.avatar}}/>
                            {(data.find_me.length > 0) && <View style={{
                                position: 'absolute',
                                top: 0,
                                left: 0,
                                padding: 12,
                                backgroundColor: "rgba(123,10,52,0.5)"
                            }}><Thumbnail medium square
                                          source={require('../assets/img/findme/findme_icon.png')}/></View>}
                            <H1 style={[styles.mediumFont, {paddingLeft: 10}]}>{data.name}</H1>
                            <Moment element={Text} fromNow ago
                                    style={[styles.smallFont, {color: '#333'}]}>{data.birthday}</Moment>
                            <Text style={styles.smallFont}>{this.state.user.breeds[data.breed - 1].name}</Text>
                        </ListItem>}
                />
            </ScrollView>
        );

    }

    renderEmptyList() {
        return (
            <View style={styles.containerTransparent}>
                <H3 style={[styles.mediumFont, {margin: 30, textAlign: 'center'}]}>¡No tienes mascotas aún, agrega tu
                    primera mascota ahora!</H3>
            </View>
        );

    }

    render() {
        const {navigate} = this.props.navigation;
        const {params} = this.props.navigation.state;
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        {(Platform.OS !== 'android') &&
                        <Button transparent onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                            <Icon name='arrow-back' style={{color: '#fff'}}/>
                        </Button>}
                    </Left>
                    <Body>
                    <Title style={{fontFamily: "Quicksand-Medium", width: 200, color: "#fff"}}>Mascota perdida</Title>
                    </Body>
                    <Right/>
                </Header>
                {Object.keys(this.state.listViewData).length ? this.renderList() : this.renderEmptyList()}
                {(this.props.isLoading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title={this.state.alertTitle}
                    message={this.state.alertMsg}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText="Cancelar"
                    confirmText="Borrar"
                    confirmButtonColor="#7b0a34"
                    onCancelPressed={() => {
                        this.hideAlert();
                    }}
                    onConfirmPressed={() => {
                        this.deleteRow(this.state.drop);
                    }}
                />
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading: state.LoginReducer.isLoading,
        user_data: state.LoginReducer.data
    };
};
const mapDispatchToProps = {
    service_error,
    service_pending,
    service_success
};
const FindmeDogs = connect(mapStateToProps, mapDispatchToProps)(FindmeDogsScreen);

export default FindmeDogs;