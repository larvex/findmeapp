import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    ImageBackground,
    Text,
    KeyboardAvoidingView,
    BackHandler,
    Platform,
    ScrollView, ActivityIndicator,
    TouchableOpacity
} from 'react-native';
import {
    Button,
    H1,
    H2,
    H3,
    Container,
    Item,
    Icon,
    Input,
    Thumbnail,
    Body,
    Header,
    Left,
    Right,
    Title,
    Content,
    Picker, ListItem, CheckBox, Footer
} from 'native-base';
import {Col, Row, Grid} from "react-native-easy-grid";
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import Camera from 'react-native-camera';
import {service_success, service_error, service_pending} from "../actions/actionCreator";
import DatePicker from 'react-native-datepicker';
import Autocomplete from 'react-native-autocomplete-input';
import AwesomeAlert from 'react-native-awesome-alerts';
import Auth from '../utils/auth';
const styles = require("./../assets/styles/styles");

class DogCaptureScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            camera: {
                type: Camera.constants.Type.back,
            },
            dog: {
                avatar: null,
                adoption: false,
                in_heat: false,
                sex: "1",
                breed: 1
            },
            user: props.user_data,
            query: '',
            showAlert: false,
            alertMsg: '',
            alertTitle: ''
        };
    }

    static navigationOptions = {
        header: null
    };

    setImage = (path) =>{
        this.setState({
            dog: {
                ...this.state.dog,
                avatar: path
            }
        });
    }

    showAlert = (msg, title) => {
        this.setState({
            showAlert: true,
            alertMsg: msg,
            alertTitle: title
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };

    navigate = (routeName, params = {}) => {
        const nav = NavigationActions.navigate({
            routeName,
            params
        });
        this.props.navigation.dispatch(nav);
    };

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };

    onSexChange(value: string) {
        this.setState({
            dog: {
                ...this.state.dog,
                sex: value
            }
        });
    }

    onBreedChange(value: string) {
        this.setState({
            dog: {
                ...this.state.dog,
                breed: value
            }
        });
    }

    adoptionCheck() {
        this.setState({
            dog: {
                ...this.state.dog,
                adoption: !this.state.dog.adoption
            }
        });
    }

    inHeatCheck() {
        this.setState({
            dog: {
                ...this.state.dog,
                in_heat: !this.state.dog.in_heat
            }
        });
    }

    switchType = () => {
        let newType;
        const {back, front} = Camera.constants.Type;

        if (this.state.camera.type === back) {
            newType = front;
        } else if (this.state.camera.type === front) {
            newType = back;
        }

        this.setState({
            camera: {
                ...this.state.camera,
                type: newType,
            },
        });
    };

    takePicture() {
        const options = {};
        //options.location = ...
        this.camera.capture({metadata: options})
            .then((data) => {
                this.setState({
                    dog: {
                        ...this.state.dog,
                        avatar: data.path
                    }
                });
            })
            .catch(err => console.error(err));
    }

    renderCamera() {
        return (
            <View style={styles.camera}>
                <Camera
                    ref={(cam) => {
                        this.camera = cam;
                    }}
                    style={styles.preview}
                    aspect={Camera.constants.Aspect.fill}
                    playSoundOnCapture={true}
                    captureTarget={Camera.constants.CaptureTarget.disk}
                    captureQuality={Camera.constants.CaptureQuality.photo}
                    type={this.state.camera.type}
                >
                    <Button rounded transparent style={styles.capture} onPress={this.takePicture.bind(this)}>

                        <Icon style={styles.takeIcon} name='camera'/>
                    </Button>
                    <Button rounded transparent style={styles.typeBtn} onPress={() => this.switchType()}>
                        <Icon style={styles.typeIcon} name='reverse-camera'/>
                    </Button>
                    <Button rounded transparent style={styles.photosBtn} onPress={() => this.navigate('CameraRollS',  {
                        setImage: this.setImage.bind(this)
                    })}>
                        <Icon style={styles.photosIcon} name='images'/>
                    </Button>
                </Camera>
            </View>
        );
    }

    renderImage() {
        return (
            <View style={styles.camera}>
                <Image
                    //source={require('../assets/img/addpet/dog.png')}
                    source={{uri: this.state.dog.avatar}}
                    style={styles.preview}
                />
                <Button rounded transparent style={styles.cancelBtn} onPress={() => this.setState({
                    dog: {
                        ...this.state.dog,
                        avatar: null
                    }
                })}>
                    <Icon style={styles.cancelIcon} name='trash'/>
                </Button>
            </View>
        );
    }

    saveDog() {


        if (!this.state.dog.name || !this.state.dog.birthday || !this.state.dog.bio) {
            this.showAlert('', 'Debes llenar todos los campos.');
            return;
        }

        if (!this.state.dog.avatar) {
            this.showAlert('', 'Debes elegir una foto de tu mascota.');
            return;
        }

        let form = new FormData();
        form.append('name', this.state.dog.name);
        form.append('breed', this.state.dog.breed);
        form.append('birthday', this.state.dog.birthday);
        form.append('sex', this.state.dog.sex);
        form.append('bio', this.state.dog.bio);
        form.append('adoption', (this.state.dog.adoption) ? 1 : 0);
        form.append('in_heat', (this.state.dog.in_heat) ? 1 : 0);
        form.append('avatar', {uri: this.state.dog.avatar, name: 'avatar.jpg', type: 'image/jpg'});

        console.log(form);

        this.props.service_pending();
        let data = {};
        Auth.saveDog(form, this.state.user.bearer).then((resp) => {

            let pet = resp.data;
            let pets = this.state.user.pets;
            pets.push(pet);
            this.props.service_success({pets});
            console.log('got data', pet);
            this.props.navigation.dispatch(NavigationActions.back());


        }).catch(error => {
            console.log(error.response);
            this.props.service_error();
        });
    }

    getBreeds() {
        const {breeds} = this.state.user;
        return breeds.map((item, i) => {
            return (
                <Picker.Item key={item.id} label={item.name} value={item.id}/>
            );
        });
    }

    render() {
        //const { query } = this.state;
        //const data = this._filterData(query)

        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        {(Platform.OS !== 'android') &&
                        <Button transparent onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                            <Icon name='arrow-back' style={{color: '#fff'}}/>
                        </Button>}

                    </Left>
                    <Body>
                    <Title style={{fontFamily: "Quicksand-Medium", width: 200, color: '#fff'}}>Agregar Mascota</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    {this.state.dog.avatar ? this.renderImage() : this.renderCamera()}
                    <Grid style={styles.containerTransparent}>
                        <Row style={{height: 60}}>
                            <Col>
                                <Item style={styles.input}>
                                    <Input style={styles.placeholder} placeholder='Nombre'
                                           value={this.state.dog.name}
                                           onChangeText={(text) => this.setState({
                                               dog: {
                                                   ...this.state.dog,
                                                   name: text
                                               }
                                           })}/>
                                </Item>
                            </Col>
                            <Col>
                                <Picker
                                    mode="dropdown"
                                    placeholder="Raza"
                                    selectedValue={this.state.dog.breed}
                                    onValueChange={this.onBreedChange.bind(this)}
                                >
                                    {this.getBreeds()}
                                </Picker>
                            </Col>


                        </Row>
                        <Row style={{height: 60}}>
                            <Item style={styles.input}>
                                <Col>
                                    <DatePicker
                                        style={{width: 200}}
                                        date={this.state.dog.birthday}
                                        mode="date"
                                        placeholder="Cumpleaños"
                                        format="YYYY-MM-DD"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                marginLeft: 36,
                                                marginRight: 46,
                                            }
                                            // ... You can check the source to find the other keys.
                                        }}
                                        onDateChange={(date) => {
                                            this.setState({
                                                dog: {
                                                    ...this.state.dog,
                                                    birthday: date
                                                }
                                            })
                                        }}
                                    />
                                </Col>
                                <Col>
                                    <Picker
                                        mode="dropdown"
                                        placeholder="Sexo"
                                        selectedValue={this.state.dog.sex}
                                        onValueChange={this.onSexChange.bind(this)}
                                    >
                                        <Item label="Hembra" value="1"/>
                                        <Item label="Macho" value="2"/>
                                    </Picker>
                                </Col>
                            </Item>

                        </Row>
                        <Row style={{height: 60}}>
                            <Item style={styles.input}>
                                <Input style={styles.placeholder} placeholder='Biografía'
                                       value={this.state.dog.bio}
                                       onChangeText={(text) => this.setState({
                                           dog: {
                                               ...this.state.dog,
                                               bio: text
                                           }

                                       })}
                                       maxLength={200}/>
                            </Item>
                        </Row>
                        <Row style={{height: 60}}>
                            <Col>
                                <Item style={styles.input} onPress={ () => this.adoptionCheck() }>
                                    <CheckBox checked={this.state.dog.adoption} color={"#7b0a34"}/>
                                    <Body>
                                    <Text>Adopción</Text>
                                    </Body>
                                </Item>
                            </Col>
                            <Col>
                                <Item style={styles.input} onPress={ () => this.inHeatCheck() }>
                                    <CheckBox checked={this.state.dog.in_heat} color={"#7b0a34"}/>
                                    <Body>
                                    <Text>Busca pareja</Text>
                                    </Body>
                                </Item>
                            </Col>

                        </Row>
                    </Grid>

                </Content>
                <Footer>
                    <Button disabled={this.props.isLoading} rounded light style={styles.galleryBtn} onPress={() => this.saveDog()}>
                        <H3 style={[styles.mediumFont, {color: "#fff", height: 30}]}>Guardar</H3>
                    </Button>
                </Footer>
                {(this.props.isLoading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title={this.state.alertTitle}
                    message={this.state.alertMsg}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={false}
                    showConfirmButton={true}
                    confirmText="Entendido"
                    confirmButtonColor="#7b0a34"
                    onConfirmPressed={() => {
                        this.hideAlert();
                    }}
                />
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading: state.LoginReducer.isLoading,
        user_data: state.LoginReducer.data
    };
};
const mapDispatchToProps = {
    service_error,
    service_pending,
    service_success
};
const DogCapture = connect(mapStateToProps, mapDispatchToProps)(DogCaptureScreen);

export default DogCapture;