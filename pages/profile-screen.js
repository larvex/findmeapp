import React, {Component} from 'react';
import {StyleSheet, View, Image, ImageBackground, Text, BackHandler } from 'react-native';
import {Button, H1, H2, H3, Container, Item, Icon, Input, Thumbnail, Body, Header, Left, Right, Title, SwipeRow, Fab} from 'native-base';
import {Col, Row, Grid} from "react-native-easy-grid";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import { logout } from "../actions/actionCreator";
import Auth from '../utils/auth';
import AwesomeAlert from 'react-native-awesome-alerts';



const styles = require("./../assets/styles/styles");

class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user:props.user_data,
            active:false,
            showAlert: false,
            alertMsg: '',
            alertTitle: '',
            showModal: false,
        };
    }

    userLogout(){
        Auth.logout()
            .then(()=>{
                this.props.logout();
            }).catch(error => {
            console.log(error);
        });
    }
    static navigationOptions = {
        tabBarLabel: 'Perfil',
        // Note: By default the icon is only shown on iOS. Search the showIcon option below.
        tabBarIcon: ({tintColor}) => (
            <Icon name="person" style={{color:tintColor}} />
        ),
        header:null
    };
    componentDidMount(){
        if(
            !this.state.user.name ||
            !this.state.user.lastname ||
            !this.state.user.user_data.state_id ||
            !this.state.user.user_data.street ||
            !this.state.user.user_data.suburb ||
            !this.state.user.user_data.whatsapp ||
            !this.state.user.user_data.zip
        ){
            this.showAlert('Completa tu perfil para disfrutar Find Me y todas sus características', 'Perfil incompleto');
        }
    }
    navigate = (routeName) => {
        const nav = NavigationActions.navigate({
            routeName
        });
        this.props.navigation.dispatch(nav);
    };
    showAlert = (msg, title) => {

        this.setState({
            showAlert: true,
            alertMsg: msg,
            alertTitle: title
        });
    };
    hideAlert = () => {
        this.setState({
            showAlert: false,
            alertMsg: '',
            alertTitle: '',
        });
    };
    render() {
        let picUri = {uri: Auth.url+'/storage/images/'+this.props.user_data.user_data.avatar};
        return (
            <Container>
                <Header style={styles.header}>
                    <Left></Left>
                    <Body>
                    <Title style={{fontFamily: "Quicksand-Medium", width: 200, color:"#fff"}}>Find Me</Title>
                    </Body>
                    <Right>
                        <Icon style={{color: "#fff"}} name='notifications' onPress={() => this.navigate("Notifications")}/>
                    </Right>
                </Header>
                    <ImageBackground
                        style={{
                            flex: 1,
                            justifyContent:'center',
                            alignItems:'center',
                            paddingTop:5
                        }}
                        source={require('../assets/img/profile/profile_back.png')}>
                        <Grid style={[styles.containerTransparent,{padding:5}]}>
                            <Row size={1}>
                                <Col size={1}>
                                    <Thumbnail large style={{borderColor:"#7b0a34", borderWidth:3}} source={picUri} />
                                </Col>
                                <Col size={2}>
                                    <H3 style={[styles.mediumFont,{color:"#fff", height:30}]} >{this.props.user_data.name} {this.props.user_data.lastname}</H3>
                                    <H3 style={[styles.smallFont,{color:"#fff"}]} >{this.props.user_data.email}</H3>
                                    <H3 style={[styles.smallFont,{color:"#fff"}]} >{this.props.user_data.user_data.whatsapp}</H3>
                                </Col>
                                <Col size={1}>
                                    <Fab
                                        active={this.state.active}
                                        direction="down"
                                        containerStyle={{ margin:-10 }}
                                        style={{ backgroundColor: '#7b0a34'}}
                                        position="topRight"
                                        onPress={() => this.setState({ active: !this.state.active })}>
                                        <Icon name="menu" />
                                        <Button style={{ backgroundColor: 'transparent' }} onPress={() => {
                                            this.setState({ active: !this.state.active }, this.navigate("Messages"));
                                        }}>
                                            <Icon name="chatbubbles" />
                                        </Button>
                                        <Button style={{ backgroundColor: 'transparent' }} onPress={() => {
                                            this.setState({ active: !this.state.active }, this.navigate("ProfileEdit"));

                                        }}>
                                            <Icon name="create" />
                                        </Button>
                                        <Button style={{ backgroundColor: 'transparent' }} onPress={() => {
                                            this.setState({ active: !this.state.active },this.userLogout());

                                        }}>
                                            <Icon name="power" />
                                        </Button>
                                    </Fab>
                                </Col>
                            </Row>
                        </Grid>
                    </ImageBackground>
                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title={this.state.alertTitle}
                    message={this.state.alertMsg}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={false}
                    showConfirmButton={true}
                    confirmText="Entendido"
                    confirmButtonColor="#7b0a34"
                    onCancelPressed={() => {
                        this.hideAlert();
                    }}
                    onConfirmPressed={() => {
                        this.hideAlert();
                        this.navigate("ProfileEdit")
                    }}
                />
            </Container>
        );
    }
}
const mapStateToProps = state => ({
    user_data: state.LoginReducer.data
});

const mapDispatchToProps = {
    logout
};

const Profile = connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);

export default Profile;