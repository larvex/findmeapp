import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {ScrollView, Text, View, TouchableOpacity} from 'react-native';
import {H1, H2, H3, Thumbnail, Button} from 'native-base';


const styles = require("./../assets/styles/styles");

class SideMenu extends Component {
    navigateToScreen = (route) => () => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.dispatch(navigateAction);
    }

    render() {
        return (
            <View style={styles.containerNav}>
                <ScrollView>
                    <TouchableOpacity onPress={this.navigateToScreen('Profile')}>
                        <View style={[styles.navSectionStyle, styles.sectionBottomDash]}>
                            <Thumbnail medium source={require('../assets/img/profile/profile_pic.png')}/>
                            <H3 style={[styles.navItemStyle, styles.mediumFont]}>
                                Paola Mora
                            </H3>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.navigateToScreen('Pets')}>
                        <View style={styles.navSectionStyle}>
                            <Thumbnail medium source={require('../assets/img/icons/pets_icon.png')}/>
                            <H3 style={styles.navItemStyle}>
                                Mis mascotas
                            </H3>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.navigateToScreen('FoundPet')}>
                        <View style={styles.navSectionStyle}>
                            <Thumbnail medium source={require('../assets/img/icons/find_icon.png')}/>
                            <H3 style={styles.navItemStyle}>
                                Alerta Find Me
                            </H3>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.navigateToScreen('FindLove')}>
                        <View style={styles.navSectionStyle}>
                            <Thumbnail medium source={require('../assets/img/icons/love_icon.png')}/>
                            <H3 style={styles.navItemStyle}>
                                Find Me Love
                            </H3>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.navigateToScreen('FindHome')}>
                        <View style={styles.navSectionStyle}>
                            <Thumbnail medium source={require('../assets/img/icons/home_icon.png')}/>
                            <H3 style={styles.navItemStyle}>
                                Find Me Home
                            </H3>
                        </View>
                    </TouchableOpacity>

                </ScrollView>
                <TouchableOpacity onPress={this.navigateToScreen('Login')}>
                    <View style={styles.footerContainer}>
                        <Thumbnail medium source={require('../assets/img/icons/logout_icon.png')}/>
                        <H3 style={styles.navItemStyle}>
                            Cerrar Sesión
                        </H3>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

SideMenu.propTypes = {
    navigation: PropTypes.object
};

export default SideMenu;