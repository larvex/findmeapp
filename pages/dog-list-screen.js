import React, {Component} from 'react';
import moment from 'moment/min/moment-with-locales';
import Moment from 'react-moment';
import {
    StyleSheet,
    View,
    Image,
    ImageBackground,
    Text,
    ScrollView,
    ListView,
    TouchableOpacity,
    BackHandler,
    Platform,
    ActivityIndicator
} from 'react-native';
import {
    Button,
    H1,
    H2,
    H3,
    Container,
    Item,
    Icon,
    Body,
    Input,
    Thumbnail,
    List,
    ListItem,
    Header,
    Left,
    Right,
    Title, Content, Footer
} from 'native-base';
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import {service_success, service_error, service_pending} from "../actions/actionCreator";
import Auth from '../utils/auth';
import AwesomeAlert from 'react-native-awesome-alerts';
import Modal from "react-native-modal";
import Toast from 'react-native-simple-toast';

Moment.globalMoment = moment;
Moment.globalLocale = 'es';


const styles = require("./../assets/styles/styles");

class DogListScreen extends Component {

    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            basic: true,
            listViewData: props.user_data.pets,
            user: props.user_data,
            showAlert: false,
            alertMsg: '',
            alertTitle: '',
            showModal: false,
            modalData: {
                name: '',
                avatar: '',
                breed: 1,
                find_me: {}

            }
        };
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };
    deleteRow(drop) {
        if (drop !== false) {
            let {secId, rowId, rowMap, id} = drop;
            this.props.service_pending();
            Auth.dropDog(id, this.state.user.bearer).then((resp) => {

                let pets = resp.data;
                this.props.service_success({pets});

                rowMap[`${secId}${rowId}`].props.closeRow();
                //const newData = [...this.state.listViewData];
                //newData.splice(rowId, 1);

                this.setState({listViewData: pets, drop:false});


            }).catch(error => {
                console.log(error.response);
                this.props.service_error();
                this.setState({drop:false});

            });
        }


    }


    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel: 'Mascotas',
            // Note: By default the icon is only shown on iOS. Search the showIcon option below.
            tabBarIcon: ({tintColor}) => (
                <Icon name="paw" style={{color: tintColor}}/>
            ),
            header: null,
            //tabBarOnPress: (data) => navigation.state.params.tabBarPressCb(data, navigation.state.params._this),
        }
    };

    navigate = (routeName, params = {}) => {
        const nav = NavigationActions.navigate({
            routeName,
            params
        });
        this.props.navigation.dispatch(nav);
    };

    showAlert = (msg, title, secId, rowId, rowMap, id) => {

        //this.deleteRow(secId, rowId, rowMap, data.id)

        this.setState({
            showAlert: true,
            alertMsg: msg,
            alertTitle: title,
            drop: {secId, rowId, rowMap, id}
        });
    };

    openInfoModal(data) {
        this.setState({
            showModal: true,
            modalData: data
        });
    };

    hideModal = () => {
        this.setState({
            showModal: false,
            modalData: {
                name: '',
                avatar: '',
                breed: 1,
                find_me: {}
            }
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false,
            alertMsg: '',
            alertTitle: '',
        });
    };

    markDogFound = () =>{
        let data = {
            pet_id: this.state.modalData.id,
        };
        this.props.service_pending();


        Auth.closeAlert(data, this.state.user.bearer).then((resp) => {

            let pet = resp.data;
            console.log('got data', pet);

            let pets = this.state.user.pets;
            pets.splice(this.state.modalData.rowId, 1, pet);
            this.props.service_success({pets});
            this.hideModal();
            Toast.show('Se desactivo la alerta Find Me', Toast.LONG);


        }).catch(error => {
            console.log(error.response);
            this.props.service_error();
        });
    };

    renderList() {
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        return (
            <ScrollView>
                <List
                    dataSource={ds.cloneWithRows(this.state.listViewData)}
                    renderRow={(data, secId, rowId, rowMap) =>
                        <ListItem style={styles.listItemPets} onPress={_ => this.openInfoModal({...data, secId, rowId, rowMap})}>
                            <Thumbnail large square
                                       source={{uri: Auth.url+'/storage/images/' + data.avatar}}/>
                            {(data.find_me.length > 0) && <View style={{
                                position: 'absolute',
                                top: 0,
                                left: 0,
                                padding: 12,
                                backgroundColor: "rgba(123,10,52,0.5)"
                            }}><Thumbnail medium square
                                          source={require('../assets/img/findme/findme_icon.png')}/></View>}
                            <View style={{flex:1, flexDirection:'row', justifyContent:'flex-start', alignItems:'baseline'}} >
                                <Text style={[styles.mediumFont, {
                                    paddingLeft: 10,
                                    fontSize: 25,
                                    color: '#202020'
                                }]}>{data.name}</Text>
                                <Moment element={Text} fromNow ago
                                        style={[styles.smallFont, {color: '#333'}]}>{data.birthday}</Moment>
                                <Text style={styles.smallFont}>{this.state.user.breeds[data.breed - 1].name}</Text>
                            </View>
                        </ListItem>}
                    renderLeftHiddenRow={(data, secId, rowId, rowMap) =>
                        <Button full light style={{backgroundColor: "#7b0a34", width: 80}}
                                onPress={_ => this.navigate('EditPet', {...data, rowId})}>
                            <Icon style={{color: "#fff"}} active name="create"/>
                        </Button>}
                    renderRightHiddenRow={(data, secId, rowId, rowMap) =>
                        <Button full light style={{backgroundColor: "#5d022f", width: 80}}
                                onPress={_ => this.showAlert('', '¿Deseas borrar a ' + data.name + ' de tu lista de mascotas?', secId, rowId, rowMap, data.id)}>
                            <Icon style={{color: "#fff"}} active name="trash"/>
                        </Button>
                    }
                    leftOpenValue={80}
                    rightOpenValue={-80}
                />
            </ScrollView>
        );

    }

    renderEmptyList() {
        return (
            <View style={styles.containerTransparent}>
                <H3 style={[styles.mediumFont, {margin: 30, textAlign: 'center'}]}>¡No tienes mascotas aún, agrega tu
                    primera mascota ahora!</H3>
            </View>
        );

    }

    render() {
        const {navigate} = this.props.navigation;
        const {params} = this.props.navigation.state;
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        {(Platform.OS !== 'android') &&
                        <Button transparent onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                            <Icon name='arrow-back' style={{color: '#fff'}}/>
                        </Button>}
                    </Left>
                    <Body>
                    <Title style={{fontFamily: "Quicksand-Medium", width: 200, color: "#fff"}}>Mis mascotas</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    {Object.keys(this.state.listViewData).length ? this.renderList() : this.renderEmptyList()}
                </Content>
                <Footer>
                    <Button light style={styles.galleryBtn}
                            onPress={() => this.navigate('AddPet')}>
                        <H3 style={[styles.mediumFont, {color: "#fff", height: 30}]}>Agregar Mascota</H3>
                    </Button>
                </Footer>
                {(this.props.isLoading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title={this.state.alertTitle}
                    message={this.state.alertMsg}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText="Cancelar"
                    confirmText="Borrar"
                    confirmButtonColor="#7b0a34"
                    onCancelPressed={() => {
                        this.hideAlert();
                    }}
                    onConfirmPressed={() => {
                        this.deleteRow(this.state.drop);
                    }}
                />
                <Modal
                    isVisible={this.state.showModal}
                    onBackButtonPress={()=>this.setState({
                        showModal: false,
                    })}
                    onBackdropPress={()=>this.setState({
                        showModal: false,
                    })}
                >
                    <View style={{backgroundColor: '#fff', borderRadius: 15, alignItems: 'center'}}>
                        <View style={{paddingLeft: 30, paddingRight: 30, paddingTop: 5, paddingBottom: 5}}>
                            <Text style={{
                                textAlign: 'center',
                                fontFamily: "Quicksand-Medium",
                                color: "#333",
                                fontSize: 30,
                                paddingRight: 15
                            }}>{this.state.modalData.name}
                                <Text style={[styles.mediumFont, {color: "#666", fontSize: 20}]}> <Moment element={Text}
                                                                                                          fromNow
                                                                                                          ago>{this.state.modalData.birthday}</Moment></Text>
                            </Text>
                        </View>
                        <View style={{width: '100%', borderBottomWidth: 1, borderBottomColor: '#ccc'}}/>
                        <Image style={styles.image}
                               source={{uri: Auth.url+'/storage/images/' + this.state.modalData.avatar}}/>
                        <View style={{justifyContent: 'flex-start', width: '90%', margin: 10, flexDirection: 'column'}}>
                            <View style={{flexDirection: 'row', justifyContent: 'space-around', padding: 10}}>
                                <Text style={[styles.mediumFont, {
                                    color: "#666",
                                    fontSize: 20
                                }]}>{this.state.user.breeds[this.state.modalData.breed - 1].name}</Text>

                                <Icon style={{color: '#7b0a34'}}
                                      name={(this.state.modalData.sex === 1) ? 'female' : 'male'}/>
                            </View>
                            <View style={{flexDirection: 'row', justifyContent: 'space-around', padding: 10}}>
                                <Text style={[styles.mediumFont, {color: "#666", fontSize: 15}]}><Icon
                                    style={{color: (this.state.modalData.adoption) ? '#7b0a34' : '#dcdcdc'}}
                                    name="home"/></Text>
                                <Text style={[styles.mediumFont, {color: "#666", fontSize: 15}]}><Icon
                                    style={{color: (this.state.modalData.in_heat) ? '#7b0a34' : '#dcdcdc'}}
                                    name="heart"/></Text>
                                <Text style={[styles.mediumFont, {color: "#666", fontSize: 15}]}><Icon
                                    style={{color: (this.state.modalData.find_me.length > 0) ? '#7b0a34' : '#dcdcdc'}}
                                    name="search"/></Text>
                            </View>
                        </View>

                        {(this.state.modalData.find_me.length > 0)&&<Button light style={[styles.addPetButton, {
                            width: '100%',
                            borderBottomLeftRadius: 15,
                            borderBottomRightRadius: 15
                        }]}
                                onPress={() => this.markDogFound()}>
                            <H3 style={[styles.mediumFont, {color: "#fff", height: 30}]}>Encontré a mi mascota</H3>
                        </Button>}
                    </View>
                </Modal>
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading: state.LoginReducer.isLoading,
        user_data: state.LoginReducer.data
    };
};
const mapDispatchToProps = {
    service_error,
    service_pending,
    service_success
};
const DogList = connect(mapStateToProps, mapDispatchToProps)(DogListScreen);

export default DogList;