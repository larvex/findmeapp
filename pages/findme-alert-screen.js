import React, {Component} from 'react';
import {
    StyleSheet, View, Image, ImageBackground, Text, KeyboardAvoidingView,
    AppRegistry,
    Dimensions,
    TouchableHighlight, Alert, BackHandler, Platform, TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import {
    Button,
    H1,
    H2,
    H3,
    Container,
    Item,
    Icon,
    Input,
    Thumbnail,
    Body,
    Header,
    Left,
    Right,
    Title,
    Content, Picker, Footer
} from 'native-base';
import {Col, Row, Grid} from "react-native-easy-grid";
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import {service_success, service_error, service_pending} from "../actions/actionCreator";
import Auth from '../utils/auth';
import Toast from 'react-native-simple-toast';

const styles = require("./../assets/styles/styles");

class FoundPetScreen extends Component {

    constructor(props) {
        super(props);
        let dog = props.navigation.state.params.dog;
        console.log(dog);
        this.state = {
            dog: {
                ...dog,
                in_heat: (!!dog.in_heat),
                adoption: (!!dog.adoption),
                sex: dog.sex.toString()
            },
            user: props.user_data,
            showAlert: false,
            alertMsg: '',
            alertTitle: '',
            description: '',
            position: {
                latitude: 0,
                longitude: 0,
                set: false
            }

        };
    }

    static navigationOptions = {
        header: null
    };

    navigate = (routeName, params = {}) => {
        const nav = NavigationActions.navigate({
            routeName,
            params
        });
        this.props.navigation.dispatch(nav);
    };

    setLocation(position) {
        this.setState({
            position: {
                ...position,
                set: true
            }
        });
        console.log('ubicacion salvada', position);
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };

    createAlert() {
        if (!this.state.description) {
            Toast.show('Debes llenar todos los campos', Toast.LONG);
            return;
        }
        if (!this.state.position.set) {
            Toast.show('Debes proporcionar una ubicación', Toast.LONG);
            return;
        }


        let data = {
            pet_id: this.state.dog.id,
            description: this.state.description,
            lat: this.state.position.latitude,
            lng: this.state.position.longitude
        };

        this.props.service_pending();


        Auth.createAlert(data, this.state.user.bearer).then((resp) => {

            let pet = resp.data;
            console.log('got data', pet);

            let pets = this.state.user.pets;
            pets.splice(this.state.dog.rowId, 1, pet);
            this.props.service_success({pets});
            Toast.show('Se ha creado la alerta para tu mascota', Toast.LONG);
            this.props.navigation.dispatch(NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({
                        routeName: "AppMain",
                    }),
                ],
            }));


        }).catch(error => {
            console.log(error.response);
            this.props.service_error();
        });
    }

    render() {
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        {(Platform.OS !== 'android') &&
                        <Button transparent onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                            <Icon name='arrow-back' style={{color: '#fff'}}/>
                        </Button>}
                    </Left>
                    <Body>
                    <Title style={{fontFamily: "Quicksand-Medium", width: 200, color: "#fff"}}>Mascota
                        Extraviada</Title>
                    </Body>
                    <Right/>
                </Header>
                    <Content>
                        <Image
                            style={{
                                width: '100%',
                                height: 300
                            }}
                            source={{uri: Auth.url+'/storage/images/' + this.state.dog.avatar}}/>
                        <Grid style={styles.containerTransparent}>
                            <Row style={{padding: 10}}>
                                <Col size={4}>
                                    <Item style={styles.inputTextArea}>
                                        <Input multiline={true}
                                               value={this.state.description}
                                               placeholder='Señas particulares'
                                               onChangeText={(text) => this.setState({
                                                   description: text
                                               })}
                                               maxLength={200}/>
                                    </Item>
                                </Col>
                                <Col size={1} style={{
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}>
                                    <Button rounded style={styles.locationBtn} onPress={
                                        () => this.navigate('FindMeAlertMap', {
                                            setLocation: this.setLocation.bind(this)
                                        })
                                    }>
                                        <Icon style={styles.locationIcon}
                                              name={(this.state.position.set) ? 'checkmark' : 'pin'}/>
                                    </Button>
                                </Col>
                            </Row>
                        </Grid>
                    </Content>
                    <Footer style={{backgroundColor: 'green'}}>
                        <Button disabled={this.props.isLoading} rounded light style={styles.galleryBtn} onPress={() => this.createAlert()}>
                            <H3 style={[styles.mediumFont, {color: "#fff", height: 30}]}>Crear alerta</H3>
                        </Button>
                    </Footer>
                {(this.props.isLoading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading: state.LoginReducer.isLoading,
        user_data: state.LoginReducer.data
    };
};
const mapDispatchToProps = {
    service_error,
    service_pending,
    service_success
};
const FoundPet = connect(mapStateToProps, mapDispatchToProps)(FoundPetScreen);

export default FoundPet;
