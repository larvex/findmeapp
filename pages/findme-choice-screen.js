import React, {Component} from 'react';
import {StyleSheet, View, Image, ImageBackground, Text, ScrollView, ListView, TouchableOpacity, BackHandler, Platform} from 'react-native';
import {Button, H1, H2, H3, Container, Item, Icon, Body, Input, Thumbnail, List, ListItem, Header, Left, Right, Title} from 'native-base';
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";


const styles = require("./../assets/styles/styles");

class FindMeChoiceScreen extends Component {

    static navigationOptions = {
        tabBarLabel: 'Find Me',
        // Note: By default the icon is only shown on iOS. Search the showIcon option below.
        tabBarIcon: ({tintColor}) => (
            <Icon name="search" style={{color:tintColor}} />
        ),
        header:null
    };
    navigate = (routeName) => {
        const nav = NavigationActions.navigate({
            routeName
        });
        this.props.navigation.dispatch(nav);
    };
    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };
    render() {
        return (
            <Container style={{display:'flex', flex:1}} >
                <Header style={styles.header}>
                    <Left>
                        {(Platform.OS !== 'android')&&
                        <Button transparent onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                            <Icon name='arrow-back' style={{color:'#fff'}} />
                        </Button>}

                    </Left>
                    <Body>
                    <Title style={{fontFamily:"Quicksand-Medium", width: 200,color:"#fff"}}>Find Me</Title>
                    </Body>
                    <Right/>
                </Header>
                <View style={{flex:1,flexDirection:'column', justifyContent:'center', alignContent:'center', alignItems:'center'}} >
                    <Button light style={[styles.findmeChoiceButton,{backgroundColor: '#7b0a34',}]}
                            onPress={() => this.navigate('FindMePets')}>
                        <H3 style={[styles.mediumFont,{color:"#fff", height:30}]}>¡Se perdió mi mascota!</H3>
                    </Button>
                    <Button light style={[styles.findmeChoiceButton,{backgroundColor: '#5d022f',}]}
                            onPress={() => this.navigate('FoundPet')}>
                        <H3 style={[styles.mediumFont,{color:"#fff", height:30}]}>Encontré una mascota.</H3>
                    </Button>
                </View>
            </Container>
        );
    }
}
const FindMeChoice = connect()(FindMeChoiceScreen);

export default FindMeChoice;