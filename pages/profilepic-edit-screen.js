import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    ImageBackground,
    Text,
    KeyboardAvoidingView,
    BackHandler,
    Platform,
    ScrollView, ActivityIndicator,
    TouchableOpacity
} from 'react-native';
import {
    Button,
    H1,
    H2,
    H3,
    Container,
    Item,
    Icon,
    Input,
    Thumbnail,
    Body,
    Header,
    Left,
    Right,
    Title,
    Content,
    Picker, ListItem, CheckBox
} from 'native-base';
import {Col, Row, Grid} from "react-native-easy-grid";
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import Camera from 'react-native-camera';
import {service_success, service_error, service_pending} from "../actions/actionCreator";
import DatePicker from 'react-native-datepicker';
import Autocomplete from 'react-native-autocomplete-input';
import AwesomeAlert from 'react-native-awesome-alerts';
import Auth from '../utils/auth';

const styles = require("./../assets/styles/styles");

class ProfilePicEditScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            camera: {
                type: Camera.constants.Type.back,
            },
            user: props.user_data,
            showAlert: false,
            alertMsg: '',
            alertTitle: '',
            newPic:false,
        };

    }

    static navigationOptions = {
        header: null
    };

    showAlert = (msg, title) => {
        this.setState({
            showAlert: true,
            alertMsg: msg,
            alertTitle: title
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };

    navigate = (routeName, params = {}) => {
        const nav = NavigationActions.navigate({
            routeName,
            params
        });
        this.props.navigation.dispatch(nav);
    };

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };


    switchType = () => {
        let newType;
        const {back, front} = Camera.constants.Type;

        if (this.state.camera.type === back) {
            newType = front;
        } else if (this.state.camera.type === front) {
            newType = back;
        }

        this.setState({
            camera: {
                ...this.state.camera,
                type: newType,
            },
        });
    };

    takePicture() {
        const options = {};
        //options.location = ...
        this.camera.capture({metadata: options})
            .then((data) => {
                this.setState({
                    user:{
                        ...this.state.user,
                        user_data:{
                            ...this.state.user_data,
                            avatar:data.path
                        }
                    },
                    newPic: true
                });
            })
            .catch(err => console.error(err));
    }
    setImage = (path) =>{
        this.setState({
            user:{
                ...this.state.user,
                user_data:{
                    ...this.state.user_data,
                    avatar:path
                }
            },
            newPic:true
        });
    }

    renderCamera() {
        return (
            <View style={styles.camera}>
                <Camera
                    ref={(cam) => {
                        this.camera = cam;
                    }}
                    style={styles.previewFull}
                    aspect={Camera.constants.Aspect.fill}
                    playSoundOnCapture={true}
                    captureTarget={Camera.constants.CaptureTarget.disk}
                    captureQuality={Camera.constants.CaptureQuality.photo}
                    type={this.state.camera.type}
                >
                    <Button rounded transparent style={styles.capture} onPress={this.takePicture.bind(this)}>

                        <Icon style={styles.takeIcon} name='camera'/>
                    </Button>
                    <Button rounded transparent style={styles.typeBtn} onPress={() => this.switchType()}>
                        <Icon style={styles.typeIcon} name='reverse-camera'/>
                    </Button>
                    <Button rounded transparent style={styles.photosBtn} onPress={() => this.navigate('CameraRollS',  {
                        setImage: this.setImage.bind(this)
                    })}>
                        <Icon style={styles.photosIcon} name='images'/>
                    </Button>
                </Camera>
            </View>
        );
    }

    renderImage() {
        let picUri = {uri: Auth.url+'/storage/images/'+this.state.user.user_data.avatar};
        if(this.state.newPic){
            picUri = {uri: this.state.user.user_data.avatar};
        }
        return (
            <View style={styles.camera}>
                <Image
                    //source={require('../assets/img/addpet/dog.png')}
                    source={picUri}
                    style={styles.previewFull}
                />
                <Button rounded transparent style={styles.cancelBtn} onPress={() => this.setState({
                    user:{
                        ...this.state.user,
                        user_data:{
                            ...this.state.user_data,
                            avatar:'default.png'
                        }
                    }
                })}>
                    <Icon style={styles.cancelIcon} name='trash'/>
                </Button>
            </View>
        );
    }

    renderBtn(){
        if(this.state.user.user_data.avatar !== 'default.png' || !this.state.user.user_data.avatar){
            return(
                <Button disabled={this.props.isLoading} rounded style={styles.okBtn} onPress={() => this.updatePic() }>
                    <Icon style={{color: '#fff',
                        fontWeight: '900',
                        fontSize: 50,}} name='checkmark'/>
                </Button>
            )
        }
    }

    updatePic() {

        if (!this.state.user.user_data.avatar || this.state.user.user_data.avatar === 'default') {
            this.showAlert('', 'Debes elegir una foto de perfil.');
            return;
        }

        if(!this.state.newPic){
            return;
        }
        let form = new FormData();

        form.append('avatar', {uri:this.state.user.user_data.avatar, name:'avatar.jpg', type: 'image/jpg'});


        console.log(form);

        this.props.service_pending();
        Auth.updatePic(form, this.state.user.bearer).then((resp) => {

            let user = resp.data;
            this.props.service_success(user);
            console.log('got data', user);
            this.props.navigation.dispatch(NavigationActions.back());


        }).catch(error => {
            console.log(error.response);
            this.props.service_error();
        });
    }

    render() {
        //const { query } = this.state;
        //const data = this._filterData(query)

        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        {(Platform.OS !== 'android') &&
                        <Button transparent onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                            <Icon name='arrow-back' style={{color: '#fff'}}/>
                        </Button>}

                    </Left>
                    <Body>
                    <Title style={{fontFamily: "Quicksand-Medium", width: 200, color: '#fff'}}>Foto de Perfil</Title>
                    </Body>
                    <Right/>
                </Header>
                    <View style={{flex: 1}}>
                        {(this.state.user.user_data.avatar !== 'default.png' || !this.state.user.user_data.avatar) ? this.renderImage() : this.renderCamera()}
                        {this.renderBtn()}
                    </View>
                {(this.props.isLoading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    title={this.state.alertTitle}
                    message={this.state.alertMsg}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={false}
                    showConfirmButton={true}
                    confirmText="Entendido"
                    confirmButtonColor="#7b0a34"
                    onConfirmPressed={() => {
                        this.hideAlert();
                    }}
                />
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading: state.LoginReducer.isLoading,
        user_data: state.LoginReducer.data
    };
};
const mapDispatchToProps = {
    service_error,
    service_pending,
    service_success
};
const ProfilePicEdit = connect(mapStateToProps, mapDispatchToProps)(ProfilePicEditScreen);

export default ProfilePicEdit;