import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    ImageBackground,
    Text,
    KeyboardAvoidingView,
    BackHandler,
    Platform,
    ScrollView, ActivityIndicator,
    TouchableOpacity,
    CameraRoll,
    TouchableHighlight, Dimensions, FlatList
} from 'react-native';
import {
    Button,
    H1,
    H2,
    H3,
    Container,
    Icon,
    Input,
    Thumbnail,
    Body,
    Header,
    Left,
    Right,
    Title,
    Content,
    Picker, ListItem, CheckBox, Footer
} from 'native-base';
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import {service_success, service_error, service_pending} from "../actions/actionCreator";
const styles = require("./../assets/styles/styles");
const {width} = Dimensions.get('window')
class CameraRollScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            photos: [],
            lastCursor: null,
            noMorePhotos: false,
            loading: false,
        };
    }

    static navigationOptions = {
        header: null
    };
    navigate = (routeName) => {
        const nav = NavigationActions.navigate({
            routeName
        });
        this.props.navigation.dispatch(nav);
    };

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        this.tryGetPhotos();
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress = () => {
        this.props.navigation.dispatch(NavigationActions.back());
        return true;
    };

    selectPhoto = (uri) => {
        this.props.navigation.state.params.setImage(uri);
        this.props.navigation.dispatch(NavigationActions.back());
    };

    tryGetPhotos(){
        if(!this.state.loading){
            this.setState({loading:true}, () => {
                this.getPhotos();
            })
        }
    }

    getPhotos() {

        const fetchParams = {
            first:30,
            assetType: 'All'
        };

        if(this.state.lastCursor){
            fetchParams.after = this.state.lastCursor;
        }

        CameraRoll.getPhotos(fetchParams)
            .then(r => {
                this.appendPhotos(r);
                //this.setState({photos: r.edges});
            });
    }

    appendPhotos(data){
        const photos = data.edges;
        const nextState = {
            loading: false,
        }

        if(!data.page_info.has_next_page){
            nextState.noMorePhotos = true;
        }

        if(photos.length > 0){
            nextState.lastCursor = data.page_info.end_cursor;
            nextState.photos = this.state.photos.concat(photos);
        }

        this.setState(nextState);
    }

    endReached = () => {
        if (!this.state.noMorePhotos && !this.state.loading) {
            this.tryGetPhotos();
        }
    }

    _keyExtractor = (item, index) => index;

    render() {
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        {(Platform.OS !== 'android') &&
                        <Button transparent onPress={() => this.props.navigation.dispatch(NavigationActions.back())}>
                            <Icon name='arrow-back' style={{color: '#fff'}}/>
                        </Button>}

                    </Left>
                    <Body>
                    <Title style={{fontFamily: "Quicksand-Medium", width: 200, color: '#fff'}}>Elegir Foto</Title>
                    </Body>
                    <Right/>
                </Header>
                <View style={{flex:1}}>
                    <FlatList
                        numColumns={3}
                        removeClippedSubviews={false}
                        data={this.state.photos}
                        keyExtractor={this._keyExtractor}
                        onEndReached={this.endReached}
                        onEndReachedThreshold={0.5}
                        refreshing={this.state.loading}
                        renderItem={({item, index}) =>
                        <MyListItem
                            id={index}
                            onPressItem={(uri)=>this.selectPhoto(uri)}
                            uri={item.node.image.uri}
                        />
                        }
                    />
                </View>
                {(this.state.loading) && <View style={styles.loader}>
                    <ActivityIndicator size="large"/>
                </View>}
            </Container>
        );
    }
}

class MyListItem extends React.PureComponent {
    _onPress = () => {
        this.props.onPressItem(this.props.uri);
    };

    render() {
        return (
        <TouchableHighlight
            underlayColor='transparent'
            onPress={this._onPress}
        >
            <View style={{
                width: width / 3,
                height: width / 3,
                padding: 2
            }}>
                <Image
                    style={{width: '100%', height: '100%'}}
                    source={{uri: this.props.uri}}
                />
            </View>

        </TouchableHighlight>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading: state.LoginReducer.isLoading,
        user_data: state.LoginReducer.data
    };
};
const mapDispatchToProps = {
    service_error,
    service_pending,
    service_success
};
const CameraRollS = connect(mapStateToProps, mapDispatchToProps)(CameraRollScreen);

export default CameraRollS;