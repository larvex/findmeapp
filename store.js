import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

import NavigationReducer from "./reducer/navigationReducer";
import LoginReducer from "./reducer/loginReducer";


// combineReducer applied on persisted(counterReducer) and NavigationReducer
const rootReducer = combineReducers({
    NavigationReducer,
    LoginReducer
});

const logger = createLogger();

function configureStore() {
    let store = createStore(rootReducer, compose(applyMiddleware(thunk, logger)));
    return { store };
}

export default configureStore;