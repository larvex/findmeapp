import { React } from 'react';
import { AppRegistry } from "react-native";
import { StackNavigator } from 'react-navigation';

import DogCaptureScreen  from './pages/dog-capture-screen';
import DogListScreen  from './pages/dog-list-screen';
import FindCaptureScreen  from './pages/find-capture-screen';
import FindMeAlertScreen  from './pages/find-me-alert-screen';
import FindMeListScreen  from './pages/find-me-list-screen';
import LoginScreen  from './pages/login-screen';
import MenuScreen  from './pages/menu-screen';
import MissingCaptureScreen  from './pages/missing-capture-screen';
import NotificationScreen  from './pages/notification-screen';
import ProfileScreen  from './pages/profile-edit-screen';
import RegisterScreen  from './pages/register-screen';
import TinderListScreen  from './pages/tinder-list-screen';

export const RootNavigation = StackNavigator({
    DogCapture : {
        screen: DogCaptureScreen
    },
    DogList : {
        screen: DogListScreen
    },
    FindCapture : {
        screen: FindCaptureScreen
    },
    FindMeAlert : {
        screen: FindMeAlertScreen
    },
    FindMeList : {
        screen: FindMeListScreen
    },
    Login : {
        screen: LoginScreen,
        navigationoptions:{
            title: 'Login',
            header: navigation => ({
                style: {
                    color: '#FFFFFF'
                },
                tintColor: '#0087B7'
            })
        }
    },
    Menu : {
        screen: MenuScreen
    },
    MissingCapture : {
        screen: MissingCaptureScreen
    },
    Notification : {
        screen: NotificationScreen
    },
    Profile : {
        screen: ProfileScreen
    },
    Register : {
        screen: RegisterScreen
    },
    TinderList : {
        screen: TinderListScreen
    },
});

AppRegistry.registerComponent('FindMeApp', () => RootNavigation);