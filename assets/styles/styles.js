module.exports = {
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        position: 'relative'
    },
    containerTransparent: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'transparent',
        justifyContent: 'center',
    },
    image: {
        width:'100%',
        height: 250
    },
    tabButton: {
        backgroundColor: '#2d0e2c',
        borderRadius: 0,
        height: 60,
        width:'50%'
    },
    tabText: {
        color: "#fff",
        textAlign: "center",
        width: "100%",
        fontFamily: "Quicksand-Bold",
    },
    btnText: {
        color: "#fff",
        textAlign: "center",
        width: 150,
        fontFamily: "Quicksand-Bold",
    },
    input: {
        margin: 10,
        padding: 5,
        width: "90%",
        height: 50
    },
    inputTextArea: {
        margin: 10,
        padding: 5,
        width: "90%"
    },
    button: {
        minWidth: 250,
        borderRadius: 50,
        height: 24,
        color: '#ffffff'
    },
    loginButton: {
        backgroundColor: '#dc3d40',
        width: 200,
        justifyContent: "center",
        alignSelf:'center'
    },
    addPetButton: {
        backgroundColor: '#7b0a34',
        width: "100%",
        justifyContent: "center",
        height:70,
        borderRadius:0
    },
    logoutButton: {
        backgroundColor: '#7b0a34',
        width: "100%",
        justifyContent: "center",
        height:50,
        borderRadius:0
    },
    findmeChoiceButton: {
        width: "100%",
        height:100,
        justifyContent: "center",
        borderRadius:0,
        flexGrow:1
    },
    facebookLoginButton: {
        backgroundColor: '#06699f',
        width: 200,
        justifyContent: "center"
    },
    containerNav: {
        flex: 1,
        backgroundColor: "#202020"
    },
    navItemStyle: {
        marginLeft: 20,
        color: "#fff",
        fontFamily: "Quicksand-Light"
    },
    navSectionStyle: {
        padding: 5,
        paddingTop: 10,
        flexDirection: "row",
        alignItems: "center",
        backgroundColor:'#333'
    },
    sectionBottomDash: {
        borderBottomWidth: 1,
        borderColor: "#666",
        paddingBottom: 10,

    },
    sectionHeadingStyle: {
        paddingVertical: 10,
        paddingHorizontal: 5
    },
    footerContainer: {
        padding: 10,
        backgroundColor: '#202020',
        flexDirection: "row",
        alignItems: "center",
        borderTopWidth: 1,
        borderColor: "#666",
    },
    addPetBtnContainer: {
        backgroundColor: '#202020',
        flexDirection: "row",
        alignItems: "center",
        borderTopWidth: 1,
        borderColor: "#666",
    },
    mediumFont: {
        fontFamily: "Quicksand-Medium",
        fontSize:20
    },
    smallFont: {
        fontFamily: "Quicksand-Light",
        fontSize:14,
        padding:5,
        color:"#666"
    },
    header:{
        backgroundColor:'#202020'
    },
    listItemPets:{
        height:80,
        alignItems:"center",
    },
    rightBtnView:{
        height:80
    },
    rightBtn:{
        height:40
    },
    placeholder:{
        fontFamily:"Quicksand-Medium"
    },
    camera: {
        flex: 1,
        flexDirection: 'row',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height:320,
        width:'100%'
    },
    previewFull: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        height:80,
        backgroundColor: 'transparent',
        margin: 10,
        alignSelf:"auto",
        borderRadius:30
    },
    galleryBtn: {
        backgroundColor: '#7b0a34',
        width: "100%",
        justifyContent: "center",
        height:60,
        borderRadius:0
    },
    okBtn: {
        backgroundColor: '#7b0a34',
        width: "100%",
        justifyContent: "center",
        height:70,
        borderRadius:0
    },
    cancelBtn:{
        position: 'absolute',
        right: 5,
        top: 10,
        backgroundColor: 'transparent',
        borderRadius:30
    },
    cancelIcon: {
        color: '#dc3d40',
        fontWeight: '900',
        fontSize: 35,
    },
    editBtn:{
        position: 'absolute',
        right: 5,
        top: 10,
        backgroundColor: 'transparent',
        height:50,
        width:50,
        borderRadius:30
    },
    editIcon: {
        color: '#fff',
        fontWeight: '900',
        fontSize: 25,
    },
    changeBtn:{
        position: 'absolute',
        left: 10,
        bottom: 0,
        backgroundColor: '#fff',
        height:50,
        width:50,
        borderRadius:30
    },
    changeIcon: {
        color: '#7b0a34',
        fontWeight: '900',
        fontSize: 25,
    },
    takeIcon: {
        color: '#fff',
        fontWeight: '900',
        fontSize: 60,
    },
    locationBtn:{
        backgroundColor: '#fff',
        height:50,
        width:50,
        borderRadius:30
    },
    locationIcon: {
        color: '#7b0a34',
        fontWeight: '900',
        fontSize: 25,
    },
    typeBtn: {
        position: 'absolute',
        left: 5,
        bottom: 10,
    },
    typeIcon:{
        color: '#fff',
        fontWeight: '500',
        fontSize: 25,

    },
    photosBtn: {
        position: 'absolute',
        right: 5,
        bottom: 10,
    },
    photosIcon:{
        color: '#fff',
        fontWeight: '500',
        fontSize: 25,

    },
    loader: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: 'black',
        opacity: 0.5,
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center'

    },

};